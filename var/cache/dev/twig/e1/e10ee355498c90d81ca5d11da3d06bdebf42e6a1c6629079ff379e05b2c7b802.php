<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_090b1f152a917b9f715044b30f08eed4dd30828b3a94637058ffbccb5cdae65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09701a762853f59967b7ebc19aade45c149d7016f167a9743d632841b31aa127 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09701a762853f59967b7ebc19aade45c149d7016f167a9743d632841b31aa127->enter($__internal_09701a762853f59967b7ebc19aade45c149d7016f167a9743d632841b31aa127_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_5bec48405ad6c0f1a27fe0d6d32eeff094e984ddd0a4cc0d08218c5232e1a9db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5bec48405ad6c0f1a27fe0d6d32eeff094e984ddd0a4cc0d08218c5232e1a9db->enter($__internal_5bec48405ad6c0f1a27fe0d6d32eeff094e984ddd0a4cc0d08218c5232e1a9db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_09701a762853f59967b7ebc19aade45c149d7016f167a9743d632841b31aa127->leave($__internal_09701a762853f59967b7ebc19aade45c149d7016f167a9743d632841b31aa127_prof);

        
        $__internal_5bec48405ad6c0f1a27fe0d6d32eeff094e984ddd0a4cc0d08218c5232e1a9db->leave($__internal_5bec48405ad6c0f1a27fe0d6d32eeff094e984ddd0a4cc0d08218c5232e1a9db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
