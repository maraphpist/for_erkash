<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_5c78ddec2dd0213e300968bd3cecc18c8277369c7402f3d95247e69c4acc98c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8eb9ccedbac0f1d9ee0693649f60fcb8640ec26465fa8f65bc63a2312984c00c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8eb9ccedbac0f1d9ee0693649f60fcb8640ec26465fa8f65bc63a2312984c00c->enter($__internal_8eb9ccedbac0f1d9ee0693649f60fcb8640ec26465fa8f65bc63a2312984c00c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_8ef6964ecb5e3ade7f2f9ff94b13b910180a7b137e020648ed3203496ffee75e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ef6964ecb5e3ade7f2f9ff94b13b910180a7b137e020648ed3203496ffee75e->enter($__internal_8ef6964ecb5e3ade7f2f9ff94b13b910180a7b137e020648ed3203496ffee75e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8eb9ccedbac0f1d9ee0693649f60fcb8640ec26465fa8f65bc63a2312984c00c->leave($__internal_8eb9ccedbac0f1d9ee0693649f60fcb8640ec26465fa8f65bc63a2312984c00c_prof);

        
        $__internal_8ef6964ecb5e3ade7f2f9ff94b13b910180a7b137e020648ed3203496ffee75e->leave($__internal_8ef6964ecb5e3ade7f2f9ff94b13b910180a7b137e020648ed3203496ffee75e_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_56178d52a4faa13ae6e3fe2581787463b5ee71df0017db75ca93ea6dedfb3993 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56178d52a4faa13ae6e3fe2581787463b5ee71df0017db75ca93ea6dedfb3993->enter($__internal_56178d52a4faa13ae6e3fe2581787463b5ee71df0017db75ca93ea6dedfb3993_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_23d707a7f6c1cf3a28ae393368e9e9bb9bfeebef31fd41705ce0be7a09b9c3bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23d707a7f6c1cf3a28ae393368e9e9bb9bfeebef31fd41705ce0be7a09b9c3bc->enter($__internal_23d707a7f6c1cf3a28ae393368e9e9bb9bfeebef31fd41705ce0be7a09b9c3bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_23d707a7f6c1cf3a28ae393368e9e9bb9bfeebef31fd41705ce0be7a09b9c3bc->leave($__internal_23d707a7f6c1cf3a28ae393368e9e9bb9bfeebef31fd41705ce0be7a09b9c3bc_prof);

        
        $__internal_56178d52a4faa13ae6e3fe2581787463b5ee71df0017db75ca93ea6dedfb3993->leave($__internal_56178d52a4faa13ae6e3fe2581787463b5ee71df0017db75ca93ea6dedfb3993_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_b9d111c50c2b7fd8500daf12ff23e4c67125ae0e9b05f5447bd043f658642f1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9d111c50c2b7fd8500daf12ff23e4c67125ae0e9b05f5447bd043f658642f1d->enter($__internal_b9d111c50c2b7fd8500daf12ff23e4c67125ae0e9b05f5447bd043f658642f1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_17567dfb012f93144f08cff4ebcd25974dab7f57f49c3afd9d7116108937c198 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17567dfb012f93144f08cff4ebcd25974dab7f57f49c3afd9d7116108937c198->enter($__internal_17567dfb012f93144f08cff4ebcd25974dab7f57f49c3afd9d7116108937c198_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_17567dfb012f93144f08cff4ebcd25974dab7f57f49c3afd9d7116108937c198->leave($__internal_17567dfb012f93144f08cff4ebcd25974dab7f57f49c3afd9d7116108937c198_prof);

        
        $__internal_b9d111c50c2b7fd8500daf12ff23e4c67125ae0e9b05f5447bd043f658642f1d->leave($__internal_b9d111c50c2b7fd8500daf12ff23e4c67125ae0e9b05f5447bd043f658642f1d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
