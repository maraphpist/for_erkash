<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_27fcc32cbc727b456cb6bdc4703e93e2968b841d299cdcc212c3aff97df9e93e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c4fb6b4f76961a779c44bef58ad1c60cdceaea7ecbf173c19a619b4d7551669 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c4fb6b4f76961a779c44bef58ad1c60cdceaea7ecbf173c19a619b4d7551669->enter($__internal_1c4fb6b4f76961a779c44bef58ad1c60cdceaea7ecbf173c19a619b4d7551669_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_056627d0813b7e5744b4ba19fea701b4b4f47b57d24fcf41f773b6c514857905 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_056627d0813b7e5744b4ba19fea701b4b4f47b57d24fcf41f773b6c514857905->enter($__internal_056627d0813b7e5744b4ba19fea701b4b4f47b57d24fcf41f773b6c514857905_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_1c4fb6b4f76961a779c44bef58ad1c60cdceaea7ecbf173c19a619b4d7551669->leave($__internal_1c4fb6b4f76961a779c44bef58ad1c60cdceaea7ecbf173c19a619b4d7551669_prof);

        
        $__internal_056627d0813b7e5744b4ba19fea701b4b4f47b57d24fcf41f773b6c514857905->leave($__internal_056627d0813b7e5744b4ba19fea701b4b4f47b57d24fcf41f773b6c514857905_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_29c69ebe2e2e863d57b474f31910bdd52baa0762d95bf982b59f8224d6926a66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29c69ebe2e2e863d57b474f31910bdd52baa0762d95bf982b59f8224d6926a66->enter($__internal_29c69ebe2e2e863d57b474f31910bdd52baa0762d95bf982b59f8224d6926a66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6676c381f2d641e9cb9103320e01b02bf6ad39379fa9bde8f8a06610d6ebe0c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6676c381f2d641e9cb9103320e01b02bf6ad39379fa9bde8f8a06610d6ebe0c5->enter($__internal_6676c381f2d641e9cb9103320e01b02bf6ad39379fa9bde8f8a06610d6ebe0c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_6676c381f2d641e9cb9103320e01b02bf6ad39379fa9bde8f8a06610d6ebe0c5->leave($__internal_6676c381f2d641e9cb9103320e01b02bf6ad39379fa9bde8f8a06610d6ebe0c5_prof);

        
        $__internal_29c69ebe2e2e863d57b474f31910bdd52baa0762d95bf982b59f8224d6926a66->leave($__internal_29c69ebe2e2e863d57b474f31910bdd52baa0762d95bf982b59f8224d6926a66_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
