<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_9ec924366e2f87664ba94044a4897bf66d2b63a663b41eee1f997752f7076340 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f5fff4b8630102b9e87b0540d4cf714c3a3b2d6b0d2bc0df50b37a2ab9cdb40 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f5fff4b8630102b9e87b0540d4cf714c3a3b2d6b0d2bc0df50b37a2ab9cdb40->enter($__internal_2f5fff4b8630102b9e87b0540d4cf714c3a3b2d6b0d2bc0df50b37a2ab9cdb40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_7d2673606e979d1dc5e1ad6ead896f48fde5925bd44a20a68b8e1bd921e6d45a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d2673606e979d1dc5e1ad6ead896f48fde5925bd44a20a68b8e1bd921e6d45a->enter($__internal_7d2673606e979d1dc5e1ad6ead896f48fde5925bd44a20a68b8e1bd921e6d45a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_2f5fff4b8630102b9e87b0540d4cf714c3a3b2d6b0d2bc0df50b37a2ab9cdb40->leave($__internal_2f5fff4b8630102b9e87b0540d4cf714c3a3b2d6b0d2bc0df50b37a2ab9cdb40_prof);

        
        $__internal_7d2673606e979d1dc5e1ad6ead896f48fde5925bd44a20a68b8e1bd921e6d45a->leave($__internal_7d2673606e979d1dc5e1ad6ead896f48fde5925bd44a20a68b8e1bd921e6d45a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
