<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_d1f22b4d567290d22c377e28534b534b6be4ca8229b3cd7918f74ffa5c91100c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_758c3cf03ef95c888db341f4f4219a11d423682a07526b2cafcd24161f449859 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_758c3cf03ef95c888db341f4f4219a11d423682a07526b2cafcd24161f449859->enter($__internal_758c3cf03ef95c888db341f4f4219a11d423682a07526b2cafcd24161f449859_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_334e918eb33a2534b309cff4a0cdf0e690a8871bd99e530628ee510f0683d49d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_334e918eb33a2534b309cff4a0cdf0e690a8871bd99e530628ee510f0683d49d->enter($__internal_334e918eb33a2534b309cff4a0cdf0e690a8871bd99e530628ee510f0683d49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_758c3cf03ef95c888db341f4f4219a11d423682a07526b2cafcd24161f449859->leave($__internal_758c3cf03ef95c888db341f4f4219a11d423682a07526b2cafcd24161f449859_prof);

        
        $__internal_334e918eb33a2534b309cff4a0cdf0e690a8871bd99e530628ee510f0683d49d->leave($__internal_334e918eb33a2534b309cff4a0cdf0e690a8871bd99e530628ee510f0683d49d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
