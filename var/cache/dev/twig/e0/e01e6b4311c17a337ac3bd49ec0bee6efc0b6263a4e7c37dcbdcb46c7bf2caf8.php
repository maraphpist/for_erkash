<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_c5754d36886d24c4f610130a570c8fe34f3f8f69e055e2f74e9a6a27c34170a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa7014eaa53b5ec9293506576fec217a671d0f20eefe0027104993e22b7aa7ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa7014eaa53b5ec9293506576fec217a671d0f20eefe0027104993e22b7aa7ba->enter($__internal_fa7014eaa53b5ec9293506576fec217a671d0f20eefe0027104993e22b7aa7ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_dba12de405262bd7193fa5a8c7b14b669fdd9479923f4c9d33703fa48cd0817f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dba12de405262bd7193fa5a8c7b14b669fdd9479923f4c9d33703fa48cd0817f->enter($__internal_dba12de405262bd7193fa5a8c7b14b669fdd9479923f4c9d33703fa48cd0817f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_fa7014eaa53b5ec9293506576fec217a671d0f20eefe0027104993e22b7aa7ba->leave($__internal_fa7014eaa53b5ec9293506576fec217a671d0f20eefe0027104993e22b7aa7ba_prof);

        
        $__internal_dba12de405262bd7193fa5a8c7b14b669fdd9479923f4c9d33703fa48cd0817f->leave($__internal_dba12de405262bd7193fa5a8c7b14b669fdd9479923f4c9d33703fa48cd0817f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
