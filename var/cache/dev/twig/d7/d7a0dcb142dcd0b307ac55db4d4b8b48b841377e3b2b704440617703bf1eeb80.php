<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_ba4eeaddd5d43926e9828b0f443f7b3eb6428665e77b8be38e5fb4fe681b7168 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ca37825fd6598d8a11fc3c35335a7ed96873c78a6f0aab7e97d0afb2f68702b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ca37825fd6598d8a11fc3c35335a7ed96873c78a6f0aab7e97d0afb2f68702b->enter($__internal_7ca37825fd6598d8a11fc3c35335a7ed96873c78a6f0aab7e97d0afb2f68702b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_8b2b57d556bd16db7daf1ff3d8d992d7c83001b465a2ffa05982b98f995b1abb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b2b57d556bd16db7daf1ff3d8d992d7c83001b465a2ffa05982b98f995b1abb->enter($__internal_8b2b57d556bd16db7daf1ff3d8d992d7c83001b465a2ffa05982b98f995b1abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_7ca37825fd6598d8a11fc3c35335a7ed96873c78a6f0aab7e97d0afb2f68702b->leave($__internal_7ca37825fd6598d8a11fc3c35335a7ed96873c78a6f0aab7e97d0afb2f68702b_prof);

        
        $__internal_8b2b57d556bd16db7daf1ff3d8d992d7c83001b465a2ffa05982b98f995b1abb->leave($__internal_8b2b57d556bd16db7daf1ff3d8d992d7c83001b465a2ffa05982b98f995b1abb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
