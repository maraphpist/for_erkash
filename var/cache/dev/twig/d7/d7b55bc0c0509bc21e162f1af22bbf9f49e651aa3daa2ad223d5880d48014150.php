<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_17ba7283a10a58f663a8c9cfa9a16567887e0ac21fd5fa929fb362fd4db8effa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_411c70eb6648a48c5ac7d4889dd2300d2a131cca63ced4221823c8432c9cc8e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_411c70eb6648a48c5ac7d4889dd2300d2a131cca63ced4221823c8432c9cc8e6->enter($__internal_411c70eb6648a48c5ac7d4889dd2300d2a131cca63ced4221823c8432c9cc8e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_4f5efb425e46a0f4e1f98651776f138f2586e27a18491e6c9d0f42839013f9e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f5efb425e46a0f4e1f98651776f138f2586e27a18491e6c9d0f42839013f9e0->enter($__internal_4f5efb425e46a0f4e1f98651776f138f2586e27a18491e6c9d0f42839013f9e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_411c70eb6648a48c5ac7d4889dd2300d2a131cca63ced4221823c8432c9cc8e6->leave($__internal_411c70eb6648a48c5ac7d4889dd2300d2a131cca63ced4221823c8432c9cc8e6_prof);

        
        $__internal_4f5efb425e46a0f4e1f98651776f138f2586e27a18491e6c9d0f42839013f9e0->leave($__internal_4f5efb425e46a0f4e1f98651776f138f2586e27a18491e6c9d0f42839013f9e0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
