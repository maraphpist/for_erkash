<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_c7a3586c769d8ac8fbeb9126643e286decb1acada38f88a003d0ee18bd3bacc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f692317d743e9dcfd2a9f06a234165efebc1802f06ff7df46dc9e9885b7eaecf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f692317d743e9dcfd2a9f06a234165efebc1802f06ff7df46dc9e9885b7eaecf->enter($__internal_f692317d743e9dcfd2a9f06a234165efebc1802f06ff7df46dc9e9885b7eaecf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_0875da8f4a9ec9653c418789234069333c0e80f692b43f2d8608db1c033e7219 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0875da8f4a9ec9653c418789234069333c0e80f692b43f2d8608db1c033e7219->enter($__internal_0875da8f4a9ec9653c418789234069333c0e80f692b43f2d8608db1c033e7219_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_f692317d743e9dcfd2a9f06a234165efebc1802f06ff7df46dc9e9885b7eaecf->leave($__internal_f692317d743e9dcfd2a9f06a234165efebc1802f06ff7df46dc9e9885b7eaecf_prof);

        
        $__internal_0875da8f4a9ec9653c418789234069333c0e80f692b43f2d8608db1c033e7219->leave($__internal_0875da8f4a9ec9653c418789234069333c0e80f692b43f2d8608db1c033e7219_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
