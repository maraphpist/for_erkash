<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_11c34f90e1aa70931d1156a33513002793bc32544b5198745360de3ad7100084 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3ffa3ee264d3dcf6cd4f9ee4f45c5a066608f87ba71fb24635469f240ac77c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3ffa3ee264d3dcf6cd4f9ee4f45c5a066608f87ba71fb24635469f240ac77c2->enter($__internal_b3ffa3ee264d3dcf6cd4f9ee4f45c5a066608f87ba71fb24635469f240ac77c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_ac19b3331d21727ee9cfce2ebb70e6169ca791bea6cdd9f5f91d1c711f532850 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac19b3331d21727ee9cfce2ebb70e6169ca791bea6cdd9f5f91d1c711f532850->enter($__internal_ac19b3331d21727ee9cfce2ebb70e6169ca791bea6cdd9f5f91d1c711f532850_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_b3ffa3ee264d3dcf6cd4f9ee4f45c5a066608f87ba71fb24635469f240ac77c2->leave($__internal_b3ffa3ee264d3dcf6cd4f9ee4f45c5a066608f87ba71fb24635469f240ac77c2_prof);

        
        $__internal_ac19b3331d21727ee9cfce2ebb70e6169ca791bea6cdd9f5f91d1c711f532850->leave($__internal_ac19b3331d21727ee9cfce2ebb70e6169ca791bea6cdd9f5f91d1c711f532850_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
