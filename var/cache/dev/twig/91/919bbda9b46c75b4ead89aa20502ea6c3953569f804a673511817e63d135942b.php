<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_0fc2ba81e0194392c15cf97444fce413a6ce66a050af6dd7499f3a078f28bb52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_815026fc36eb63b34f3241473445ae8f457bf89ba74de57feb3f9ca69915e5aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_815026fc36eb63b34f3241473445ae8f457bf89ba74de57feb3f9ca69915e5aa->enter($__internal_815026fc36eb63b34f3241473445ae8f457bf89ba74de57feb3f9ca69915e5aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_f6ee4ec4eb6c51fb1b297a2a8ecd93cdc29f06a4c801eb6da8fbe1e174a0365c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6ee4ec4eb6c51fb1b297a2a8ecd93cdc29f06a4c801eb6da8fbe1e174a0365c->enter($__internal_f6ee4ec4eb6c51fb1b297a2a8ecd93cdc29f06a4c801eb6da8fbe1e174a0365c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_815026fc36eb63b34f3241473445ae8f457bf89ba74de57feb3f9ca69915e5aa->leave($__internal_815026fc36eb63b34f3241473445ae8f457bf89ba74de57feb3f9ca69915e5aa_prof);

        
        $__internal_f6ee4ec4eb6c51fb1b297a2a8ecd93cdc29f06a4c801eb6da8fbe1e174a0365c->leave($__internal_f6ee4ec4eb6c51fb1b297a2a8ecd93cdc29f06a4c801eb6da8fbe1e174a0365c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
