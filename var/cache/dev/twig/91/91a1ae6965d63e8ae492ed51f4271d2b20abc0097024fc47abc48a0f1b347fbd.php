<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_cf5a95adbac54adef95483068906b27542e973e054111880720610620c75663d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0f42fe449b574973b62d36fc3140c6f4822f4b6c4481901714b61ce35394121 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0f42fe449b574973b62d36fc3140c6f4822f4b6c4481901714b61ce35394121->enter($__internal_a0f42fe449b574973b62d36fc3140c6f4822f4b6c4481901714b61ce35394121_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_d384854a92f14c3d03e19952a2c592edb9b6b6abdf6567aac421d3105c602f39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d384854a92f14c3d03e19952a2c592edb9b6b6abdf6567aac421d3105c602f39->enter($__internal_d384854a92f14c3d03e19952a2c592edb9b6b6abdf6567aac421d3105c602f39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0f42fe449b574973b62d36fc3140c6f4822f4b6c4481901714b61ce35394121->leave($__internal_a0f42fe449b574973b62d36fc3140c6f4822f4b6c4481901714b61ce35394121_prof);

        
        $__internal_d384854a92f14c3d03e19952a2c592edb9b6b6abdf6567aac421d3105c602f39->leave($__internal_d384854a92f14c3d03e19952a2c592edb9b6b6abdf6567aac421d3105c602f39_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1936083f675873b82eb263c926c17eec30cf3576d3d359b87367bc5f20aaa3e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1936083f675873b82eb263c926c17eec30cf3576d3d359b87367bc5f20aaa3e0->enter($__internal_1936083f675873b82eb263c926c17eec30cf3576d3d359b87367bc5f20aaa3e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e65bb4d631410489c4feeeccae672524db1593be48aa4a917337b8158e253004 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e65bb4d631410489c4feeeccae672524db1593be48aa4a917337b8158e253004->enter($__internal_e65bb4d631410489c4feeeccae672524db1593be48aa4a917337b8158e253004_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_e65bb4d631410489c4feeeccae672524db1593be48aa4a917337b8158e253004->leave($__internal_e65bb4d631410489c4feeeccae672524db1593be48aa4a917337b8158e253004_prof);

        
        $__internal_1936083f675873b82eb263c926c17eec30cf3576d3d359b87367bc5f20aaa3e0->leave($__internal_1936083f675873b82eb263c926c17eec30cf3576d3d359b87367bc5f20aaa3e0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
