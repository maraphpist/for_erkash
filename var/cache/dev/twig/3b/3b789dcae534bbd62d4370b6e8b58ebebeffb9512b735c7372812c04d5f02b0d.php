<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_a8766f8ad97ba714943c19ae7e83ef34ee29f5de36831996a90851cab62a26d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fcac86d1114e807765ac5f1c8001a32ec316572124fbeed52ce48fe668f71e2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fcac86d1114e807765ac5f1c8001a32ec316572124fbeed52ce48fe668f71e2c->enter($__internal_fcac86d1114e807765ac5f1c8001a32ec316572124fbeed52ce48fe668f71e2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_3c751e400565270bc8f918f1cacde8d71c73c8db584263cd2bea54107574a3ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c751e400565270bc8f918f1cacde8d71c73c8db584263cd2bea54107574a3ff->enter($__internal_3c751e400565270bc8f918f1cacde8d71c73c8db584263cd2bea54107574a3ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fcac86d1114e807765ac5f1c8001a32ec316572124fbeed52ce48fe668f71e2c->leave($__internal_fcac86d1114e807765ac5f1c8001a32ec316572124fbeed52ce48fe668f71e2c_prof);

        
        $__internal_3c751e400565270bc8f918f1cacde8d71c73c8db584263cd2bea54107574a3ff->leave($__internal_3c751e400565270bc8f918f1cacde8d71c73c8db584263cd2bea54107574a3ff_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a5d2ae055cd9b83d72506604db599cf8571cec06ed66ce84579844dd568699c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5d2ae055cd9b83d72506604db599cf8571cec06ed66ce84579844dd568699c8->enter($__internal_a5d2ae055cd9b83d72506604db599cf8571cec06ed66ce84579844dd568699c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e464cdbb9ce3f9d8ac6b85ad777d23eacd8476f2bbb608454dd1e6ec67f0034d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e464cdbb9ce3f9d8ac6b85ad777d23eacd8476f2bbb608454dd1e6ec67f0034d->enter($__internal_e464cdbb9ce3f9d8ac6b85ad777d23eacd8476f2bbb608454dd1e6ec67f0034d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 5
            echo "        <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
    ";
        }
        // line 7
        echo "
    <form action=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        ";
        // line 9
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 10
            echo "            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
        ";
        }
        // line 12
        echo "
        <label for=\"username\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />

        <label for=\"password\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
        <label for=\"remember_me\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>

        <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </form>

    <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_request");
        echo "\">Forgot password ?</a>
";
        
        $__internal_e464cdbb9ce3f9d8ac6b85ad777d23eacd8476f2bbb608454dd1e6ec67f0034d->leave($__internal_e464cdbb9ce3f9d8ac6b85ad777d23eacd8476f2bbb608454dd1e6ec67f0034d_prof);

        
        $__internal_a5d2ae055cd9b83d72506604db599cf8571cec06ed66ce84579844dd568699c8->leave($__internal_a5d2ae055cd9b83d72506604db599cf8571cec06ed66ce84579844dd568699c8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 25,  97 => 22,  92 => 20,  85 => 16,  80 => 14,  76 => 13,  73 => 12,  67 => 10,  65 => 9,  61 => 8,  58 => 7,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}{% trans_default_domain 'FOSUserBundle' %}
    {% if error %}
        <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}

    <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
        {% if csrf_token %}
            <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
        {% endif %}

        <label for=\"username\">{{ 'security.login.username'|trans }}</label>
        <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />

        <label for=\"password\">{{ 'security.login.password'|trans }}</label>
        <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

        <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
        <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>

        <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" />
    </form>

    <a href=\"{{ path('fos_user_resetting_request') }}\">Forgot password ?</a>
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
