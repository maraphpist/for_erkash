<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_ba0aca08246e24f2528322e232d4de5528dafa300cad831a16f152ed5a73d85b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_621a833a7b9a2ac12f6682196c9017a11dc053c5c12667fee3bc125e337246b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_621a833a7b9a2ac12f6682196c9017a11dc053c5c12667fee3bc125e337246b3->enter($__internal_621a833a7b9a2ac12f6682196c9017a11dc053c5c12667fee3bc125e337246b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_63d79da26f1eda9b72c9da5d7a7dd40050ad13eae4f2f3617bd239729b494bd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63d79da26f1eda9b72c9da5d7a7dd40050ad13eae4f2f3617bd239729b494bd8->enter($__internal_63d79da26f1eda9b72c9da5d7a7dd40050ad13eae4f2f3617bd239729b494bd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_621a833a7b9a2ac12f6682196c9017a11dc053c5c12667fee3bc125e337246b3->leave($__internal_621a833a7b9a2ac12f6682196c9017a11dc053c5c12667fee3bc125e337246b3_prof);

        
        $__internal_63d79da26f1eda9b72c9da5d7a7dd40050ad13eae4f2f3617bd239729b494bd8->leave($__internal_63d79da26f1eda9b72c9da5d7a7dd40050ad13eae4f2f3617bd239729b494bd8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_750ab919fb3645cc04bb43d20d2a54448834f305e6c6b9ede67cd8ab3b399118 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_750ab919fb3645cc04bb43d20d2a54448834f305e6c6b9ede67cd8ab3b399118->enter($__internal_750ab919fb3645cc04bb43d20d2a54448834f305e6c6b9ede67cd8ab3b399118_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_44f2285d6591b4514ba76e5b79f32882c22280f197082dfbdd431ef53c3edec1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44f2285d6591b4514ba76e5b79f32882c22280f197082dfbdd431ef53c3edec1->enter($__internal_44f2285d6591b4514ba76e5b79f32882c22280f197082dfbdd431ef53c3edec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_44f2285d6591b4514ba76e5b79f32882c22280f197082dfbdd431ef53c3edec1->leave($__internal_44f2285d6591b4514ba76e5b79f32882c22280f197082dfbdd431ef53c3edec1_prof);

        
        $__internal_750ab919fb3645cc04bb43d20d2a54448834f305e6c6b9ede67cd8ab3b399118->leave($__internal_750ab919fb3645cc04bb43d20d2a54448834f305e6c6b9ede67cd8ab3b399118_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
