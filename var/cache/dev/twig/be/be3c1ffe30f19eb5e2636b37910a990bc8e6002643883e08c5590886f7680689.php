<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_268d95beb4061e727eea787c5670fa28b7c2b616fc053ccd560ff1dd528c3083 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26a708f968a9385d891a5869cb85a7a02231995e64ff8f7d97b057eaa5f4fa4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26a708f968a9385d891a5869cb85a7a02231995e64ff8f7d97b057eaa5f4fa4d->enter($__internal_26a708f968a9385d891a5869cb85a7a02231995e64ff8f7d97b057eaa5f4fa4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_7271d40570b7d2c0fd6dbe42507062229bddc0b8e097f02df06c13251945673e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7271d40570b7d2c0fd6dbe42507062229bddc0b8e097f02df06c13251945673e->enter($__internal_7271d40570b7d2c0fd6dbe42507062229bddc0b8e097f02df06c13251945673e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_26a708f968a9385d891a5869cb85a7a02231995e64ff8f7d97b057eaa5f4fa4d->leave($__internal_26a708f968a9385d891a5869cb85a7a02231995e64ff8f7d97b057eaa5f4fa4d_prof);

        
        $__internal_7271d40570b7d2c0fd6dbe42507062229bddc0b8e097f02df06c13251945673e->leave($__internal_7271d40570b7d2c0fd6dbe42507062229bddc0b8e097f02df06c13251945673e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
