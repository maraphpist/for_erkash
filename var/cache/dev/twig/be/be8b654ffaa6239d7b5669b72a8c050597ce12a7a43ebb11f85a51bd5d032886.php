<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_d55ec6d5b6658f9ec0f205de403505345fe05864bfb061d69827ba06242d13bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c879a9759115801ede171d50d95ce26c07d1de0c2938607b052baab8d72d2696 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c879a9759115801ede171d50d95ce26c07d1de0c2938607b052baab8d72d2696->enter($__internal_c879a9759115801ede171d50d95ce26c07d1de0c2938607b052baab8d72d2696_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_f4756aad38e7b4b0470ae768dce682ddf6d1234263902a64658029a9b3a4cd86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4756aad38e7b4b0470ae768dce682ddf6d1234263902a64658029a9b3a4cd86->enter($__internal_f4756aad38e7b4b0470ae768dce682ddf6d1234263902a64658029a9b3a4cd86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c879a9759115801ede171d50d95ce26c07d1de0c2938607b052baab8d72d2696->leave($__internal_c879a9759115801ede171d50d95ce26c07d1de0c2938607b052baab8d72d2696_prof);

        
        $__internal_f4756aad38e7b4b0470ae768dce682ddf6d1234263902a64658029a9b3a4cd86->leave($__internal_f4756aad38e7b4b0470ae768dce682ddf6d1234263902a64658029a9b3a4cd86_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_87fda2e67009b1e6398629ff0c8073e2f74aaeaeb94e598b3bdecc9cde61c4a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87fda2e67009b1e6398629ff0c8073e2f74aaeaeb94e598b3bdecc9cde61c4a9->enter($__internal_87fda2e67009b1e6398629ff0c8073e2f74aaeaeb94e598b3bdecc9cde61c4a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1c7e5e019a615b8109db5ed8fbf192fc2b09eac1dc6b3809519c4c2a76e010a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c7e5e019a615b8109db5ed8fbf192fc2b09eac1dc6b3809519c4c2a76e010a0->enter($__internal_1c7e5e019a615b8109db5ed8fbf192fc2b09eac1dc6b3809519c4c2a76e010a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_1c7e5e019a615b8109db5ed8fbf192fc2b09eac1dc6b3809519c4c2a76e010a0->leave($__internal_1c7e5e019a615b8109db5ed8fbf192fc2b09eac1dc6b3809519c4c2a76e010a0_prof);

        
        $__internal_87fda2e67009b1e6398629ff0c8073e2f74aaeaeb94e598b3bdecc9cde61c4a9->leave($__internal_87fda2e67009b1e6398629ff0c8073e2f74aaeaeb94e598b3bdecc9cde61c4a9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
