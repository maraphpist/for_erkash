<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_84b08c16058d2bccaefc0a6765921c46f4fd77b6b5f5deb28d456c1c4ded102a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93ce721d2d26edf026386f27627d42ab7ee5b6bfa56706f98852b2c695e15dae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93ce721d2d26edf026386f27627d42ab7ee5b6bfa56706f98852b2c695e15dae->enter($__internal_93ce721d2d26edf026386f27627d42ab7ee5b6bfa56706f98852b2c695e15dae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_cdddc3e94eb179a7d137d4ae0a67318c00e33ca9c4094f3743440cb4ef59132b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cdddc3e94eb179a7d137d4ae0a67318c00e33ca9c4094f3743440cb4ef59132b->enter($__internal_cdddc3e94eb179a7d137d4ae0a67318c00e33ca9c4094f3743440cb4ef59132b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_93ce721d2d26edf026386f27627d42ab7ee5b6bfa56706f98852b2c695e15dae->leave($__internal_93ce721d2d26edf026386f27627d42ab7ee5b6bfa56706f98852b2c695e15dae_prof);

        
        $__internal_cdddc3e94eb179a7d137d4ae0a67318c00e33ca9c4094f3743440cb4ef59132b->leave($__internal_cdddc3e94eb179a7d137d4ae0a67318c00e33ca9c4094f3743440cb4ef59132b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:show_content.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}
