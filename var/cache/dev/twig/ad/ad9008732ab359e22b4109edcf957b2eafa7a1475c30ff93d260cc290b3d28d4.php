<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_d9e1981aa8e284b05e9caf5b759e27b33c515389824ce30e73ce4ddfcc60ec9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae97315d6f83162882461a2a43469d3c292d8875c62333d9092ec2282b6d8923 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae97315d6f83162882461a2a43469d3c292d8875c62333d9092ec2282b6d8923->enter($__internal_ae97315d6f83162882461a2a43469d3c292d8875c62333d9092ec2282b6d8923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_173b9910bdcef8ec983c637e4199d542239b9423f11e0c5fb66eb0e02f08732b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_173b9910bdcef8ec983c637e4199d542239b9423f11e0c5fb66eb0e02f08732b->enter($__internal_173b9910bdcef8ec983c637e4199d542239b9423f11e0c5fb66eb0e02f08732b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ae97315d6f83162882461a2a43469d3c292d8875c62333d9092ec2282b6d8923->leave($__internal_ae97315d6f83162882461a2a43469d3c292d8875c62333d9092ec2282b6d8923_prof);

        
        $__internal_173b9910bdcef8ec983c637e4199d542239b9423f11e0c5fb66eb0e02f08732b->leave($__internal_173b9910bdcef8ec983c637e4199d542239b9423f11e0c5fb66eb0e02f08732b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_73f36edb0c776a1b808121cb483f65b049c2eb7ffe99c65bc51f56e77f56db3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73f36edb0c776a1b808121cb483f65b049c2eb7ffe99c65bc51f56e77f56db3b->enter($__internal_73f36edb0c776a1b808121cb483f65b049c2eb7ffe99c65bc51f56e77f56db3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_93aa113c8b518b4b6b478ea032ba6d38b549ea36685b5184702fb846379d929d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93aa113c8b518b4b6b478ea032ba6d38b549ea36685b5184702fb846379d929d->enter($__internal_93aa113c8b518b4b6b478ea032ba6d38b549ea36685b5184702fb846379d929d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_93aa113c8b518b4b6b478ea032ba6d38b549ea36685b5184702fb846379d929d->leave($__internal_93aa113c8b518b4b6b478ea032ba6d38b549ea36685b5184702fb846379d929d_prof);

        
        $__internal_73f36edb0c776a1b808121cb483f65b049c2eb7ffe99c65bc51f56e77f56db3b->leave($__internal_73f36edb0c776a1b808121cb483f65b049c2eb7ffe99c65bc51f56e77f56db3b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
