<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d4bbefb3b0dd532cc5b5f0a8b92e321fb27daac20e573b0a169576677f285fe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f97c91e6b699d8d4611bed36fc2f73728fe9d553a2581af1b723e4424a581e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f97c91e6b699d8d4611bed36fc2f73728fe9d553a2581af1b723e4424a581e4->enter($__internal_4f97c91e6b699d8d4611bed36fc2f73728fe9d553a2581af1b723e4424a581e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_fc2f6365ce4e3440fd489a498838adf6d3b70c2dce353724b72610496bee1f35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc2f6365ce4e3440fd489a498838adf6d3b70c2dce353724b72610496bee1f35->enter($__internal_fc2f6365ce4e3440fd489a498838adf6d3b70c2dce353724b72610496bee1f35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_4f97c91e6b699d8d4611bed36fc2f73728fe9d553a2581af1b723e4424a581e4->leave($__internal_4f97c91e6b699d8d4611bed36fc2f73728fe9d553a2581af1b723e4424a581e4_prof);

        
        $__internal_fc2f6365ce4e3440fd489a498838adf6d3b70c2dce353724b72610496bee1f35->leave($__internal_fc2f6365ce4e3440fd489a498838adf6d3b70c2dce353724b72610496bee1f35_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
