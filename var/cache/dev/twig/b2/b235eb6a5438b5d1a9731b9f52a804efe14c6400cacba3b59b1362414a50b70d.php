<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_b0585c49b20b01a3d94f4f4e7a6f23972e69e61e6939f7264194d1d425d7a7c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9ad8a408d103f6ce22e62328e9c3b182f1f2a0f7951281263cebda68e284432 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9ad8a408d103f6ce22e62328e9c3b182f1f2a0f7951281263cebda68e284432->enter($__internal_a9ad8a408d103f6ce22e62328e9c3b182f1f2a0f7951281263cebda68e284432_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_6e824915e3365b5795bd89e7bcf607ce94ac688515220e845434e98542e43834 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e824915e3365b5795bd89e7bcf607ce94ac688515220e845434e98542e43834->enter($__internal_6e824915e3365b5795bd89e7bcf607ce94ac688515220e845434e98542e43834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_a9ad8a408d103f6ce22e62328e9c3b182f1f2a0f7951281263cebda68e284432->leave($__internal_a9ad8a408d103f6ce22e62328e9c3b182f1f2a0f7951281263cebda68e284432_prof);

        
        $__internal_6e824915e3365b5795bd89e7bcf607ce94ac688515220e845434e98542e43834->leave($__internal_6e824915e3365b5795bd89e7bcf607ce94ac688515220e845434e98542e43834_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
