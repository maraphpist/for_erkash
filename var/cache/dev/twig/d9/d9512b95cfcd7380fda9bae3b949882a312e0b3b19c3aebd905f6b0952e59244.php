<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_83308ca1b8ae09e2e8878d149b1eb814f2c3872d06b0ece4682fea359edcc633 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d51093f670a8b11410d4b0639a2ef4d1aa0ebabefc0b912e6453f5f83614737e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d51093f670a8b11410d4b0639a2ef4d1aa0ebabefc0b912e6453f5f83614737e->enter($__internal_d51093f670a8b11410d4b0639a2ef4d1aa0ebabefc0b912e6453f5f83614737e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_c5b908fea7579b09da76af4b110035b58b4871c5f0cae3a8f6bc39bd047ff232 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5b908fea7579b09da76af4b110035b58b4871c5f0cae3a8f6bc39bd047ff232->enter($__internal_c5b908fea7579b09da76af4b110035b58b4871c5f0cae3a8f6bc39bd047ff232_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d51093f670a8b11410d4b0639a2ef4d1aa0ebabefc0b912e6453f5f83614737e->leave($__internal_d51093f670a8b11410d4b0639a2ef4d1aa0ebabefc0b912e6453f5f83614737e_prof);

        
        $__internal_c5b908fea7579b09da76af4b110035b58b4871c5f0cae3a8f6bc39bd047ff232->leave($__internal_c5b908fea7579b09da76af4b110035b58b4871c5f0cae3a8f6bc39bd047ff232_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7a2ffce3ab58fc5e5d9120ffe5e7862271e323eab0340081af5e0551200ec901 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a2ffce3ab58fc5e5d9120ffe5e7862271e323eab0340081af5e0551200ec901->enter($__internal_7a2ffce3ab58fc5e5d9120ffe5e7862271e323eab0340081af5e0551200ec901_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ce05bed58016a0f2095e7994202458cefa1aa1326fd937c31deff2ab859326cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce05bed58016a0f2095e7994202458cefa1aa1326fd937c31deff2ab859326cd->enter($__internal_ce05bed58016a0f2095e7994202458cefa1aa1326fd937c31deff2ab859326cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_ce05bed58016a0f2095e7994202458cefa1aa1326fd937c31deff2ab859326cd->leave($__internal_ce05bed58016a0f2095e7994202458cefa1aa1326fd937c31deff2ab859326cd_prof);

        
        $__internal_7a2ffce3ab58fc5e5d9120ffe5e7862271e323eab0340081af5e0551200ec901->leave($__internal_7a2ffce3ab58fc5e5d9120ffe5e7862271e323eab0340081af5e0551200ec901_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3f57d32a5132d70d6f51129d98002f4eaf74f3d0085d0c9aff234b6fa981c41d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f57d32a5132d70d6f51129d98002f4eaf74f3d0085d0c9aff234b6fa981c41d->enter($__internal_3f57d32a5132d70d6f51129d98002f4eaf74f3d0085d0c9aff234b6fa981c41d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5e03a5f44ff986610ddedd554a3b406ddcf078132145edeb6c2a4dedd093d45a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e03a5f44ff986610ddedd554a3b406ddcf078132145edeb6c2a4dedd093d45a->enter($__internal_5e03a5f44ff986610ddedd554a3b406ddcf078132145edeb6c2a4dedd093d45a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_5e03a5f44ff986610ddedd554a3b406ddcf078132145edeb6c2a4dedd093d45a->leave($__internal_5e03a5f44ff986610ddedd554a3b406ddcf078132145edeb6c2a4dedd093d45a_prof);

        
        $__internal_3f57d32a5132d70d6f51129d98002f4eaf74f3d0085d0c9aff234b6fa981c41d->leave($__internal_3f57d32a5132d70d6f51129d98002f4eaf74f3d0085d0c9aff234b6fa981c41d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
