<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_2d17f17f30d4f3ea6a640d73891facb6213a880f8e7e5d75994f5833324c3af7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ff538db8168b045c9e0c9ddcc47c23845d582be62276b672e1e38a0f6846b77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ff538db8168b045c9e0c9ddcc47c23845d582be62276b672e1e38a0f6846b77->enter($__internal_9ff538db8168b045c9e0c9ddcc47c23845d582be62276b672e1e38a0f6846b77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_ac8a66fee34ea3255e72ad54bb29f1bb1b1aeb0c66d0b8ba825866d75a08334a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac8a66fee34ea3255e72ad54bb29f1bb1b1aeb0c66d0b8ba825866d75a08334a->enter($__internal_ac8a66fee34ea3255e72ad54bb29f1bb1b1aeb0c66d0b8ba825866d75a08334a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ff538db8168b045c9e0c9ddcc47c23845d582be62276b672e1e38a0f6846b77->leave($__internal_9ff538db8168b045c9e0c9ddcc47c23845d582be62276b672e1e38a0f6846b77_prof);

        
        $__internal_ac8a66fee34ea3255e72ad54bb29f1bb1b1aeb0c66d0b8ba825866d75a08334a->leave($__internal_ac8a66fee34ea3255e72ad54bb29f1bb1b1aeb0c66d0b8ba825866d75a08334a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fbde1a1569f826bacd1f4eb804ff19db7054d368c63ecc8208af5aacd86cb4a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbde1a1569f826bacd1f4eb804ff19db7054d368c63ecc8208af5aacd86cb4a7->enter($__internal_fbde1a1569f826bacd1f4eb804ff19db7054d368c63ecc8208af5aacd86cb4a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9961b84e610a5c421dbe3c8652fed744f9cc0ddc8db4e648137330b98763f28d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9961b84e610a5c421dbe3c8652fed744f9cc0ddc8db4e648137330b98763f28d->enter($__internal_9961b84e610a5c421dbe3c8652fed744f9cc0ddc8db4e648137330b98763f28d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_9961b84e610a5c421dbe3c8652fed744f9cc0ddc8db4e648137330b98763f28d->leave($__internal_9961b84e610a5c421dbe3c8652fed744f9cc0ddc8db4e648137330b98763f28d_prof);

        
        $__internal_fbde1a1569f826bacd1f4eb804ff19db7054d368c63ecc8208af5aacd86cb4a7->leave($__internal_fbde1a1569f826bacd1f4eb804ff19db7054d368c63ecc8208af5aacd86cb4a7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
