<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_9fe2e90eb41f38649dbaf0d214681ed23e2a8bf3b12ee0f2f5c08f3d5f04ba59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d2b2b08438d52288c1b4f771a0be8687662f3435aa98b2ca3e4995112ac0994 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d2b2b08438d52288c1b4f771a0be8687662f3435aa98b2ca3e4995112ac0994->enter($__internal_5d2b2b08438d52288c1b4f771a0be8687662f3435aa98b2ca3e4995112ac0994_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_3f97db2c40f765dcc0f970183fa737fe70183c59256356f55cf35460663484ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f97db2c40f765dcc0f970183fa737fe70183c59256356f55cf35460663484ce->enter($__internal_3f97db2c40f765dcc0f970183fa737fe70183c59256356f55cf35460663484ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_5d2b2b08438d52288c1b4f771a0be8687662f3435aa98b2ca3e4995112ac0994->leave($__internal_5d2b2b08438d52288c1b4f771a0be8687662f3435aa98b2ca3e4995112ac0994_prof);

        
        $__internal_3f97db2c40f765dcc0f970183fa737fe70183c59256356f55cf35460663484ce->leave($__internal_3f97db2c40f765dcc0f970183fa737fe70183c59256356f55cf35460663484ce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
