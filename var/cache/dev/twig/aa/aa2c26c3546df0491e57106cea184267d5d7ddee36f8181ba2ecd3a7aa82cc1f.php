<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_44e449d7e286c49bde1eb2722d0db18bc1a44e15941931a92381e123f94f0614 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71c71fb6a3f043265beab1c5beaa9e70f10899bcb10f460a0572e8981aaa2a5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71c71fb6a3f043265beab1c5beaa9e70f10899bcb10f460a0572e8981aaa2a5c->enter($__internal_71c71fb6a3f043265beab1c5beaa9e70f10899bcb10f460a0572e8981aaa2a5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_fe7a22d7b5d0a64ae36b798d09284bb91356bbbdffdbf69a000ee3d4cb3ec667 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe7a22d7b5d0a64ae36b798d09284bb91356bbbdffdbf69a000ee3d4cb3ec667->enter($__internal_fe7a22d7b5d0a64ae36b798d09284bb91356bbbdffdbf69a000ee3d4cb3ec667_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_71c71fb6a3f043265beab1c5beaa9e70f10899bcb10f460a0572e8981aaa2a5c->leave($__internal_71c71fb6a3f043265beab1c5beaa9e70f10899bcb10f460a0572e8981aaa2a5c_prof);

        
        $__internal_fe7a22d7b5d0a64ae36b798d09284bb91356bbbdffdbf69a000ee3d4cb3ec667->leave($__internal_fe7a22d7b5d0a64ae36b798d09284bb91356bbbdffdbf69a000ee3d4cb3ec667_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
