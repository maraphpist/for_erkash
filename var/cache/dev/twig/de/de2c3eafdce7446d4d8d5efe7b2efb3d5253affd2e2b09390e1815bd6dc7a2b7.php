<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_badd646d4422676ef897103c4f6f755caf0e7ee2c31bf323e47191483cf58dc4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_abab4c5f116a31c13af24584ab2deecc71d01d1f6915b654112de4ac1d94aa91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abab4c5f116a31c13af24584ab2deecc71d01d1f6915b654112de4ac1d94aa91->enter($__internal_abab4c5f116a31c13af24584ab2deecc71d01d1f6915b654112de4ac1d94aa91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_2c7f7ea3654cc0df986927ba9c76d32d9d3b1ff39e82839acfc29840067e2b08 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c7f7ea3654cc0df986927ba9c76d32d9d3b1ff39e82839acfc29840067e2b08->enter($__internal_2c7f7ea3654cc0df986927ba9c76d32d9d3b1ff39e82839acfc29840067e2b08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_abab4c5f116a31c13af24584ab2deecc71d01d1f6915b654112de4ac1d94aa91->leave($__internal_abab4c5f116a31c13af24584ab2deecc71d01d1f6915b654112de4ac1d94aa91_prof);

        
        $__internal_2c7f7ea3654cc0df986927ba9c76d32d9d3b1ff39e82839acfc29840067e2b08->leave($__internal_2c7f7ea3654cc0df986927ba9c76d32d9d3b1ff39e82839acfc29840067e2b08_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
