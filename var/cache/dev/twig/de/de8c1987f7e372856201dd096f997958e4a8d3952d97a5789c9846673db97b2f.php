<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_010742bacd7922f8c0ef3868b50c1fd85132c12a1b910d45f0522f422e756bf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcdf294ab4efbfd2005e9be7fc1730b7946a79e876bacdb842372433a5cd2230 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcdf294ab4efbfd2005e9be7fc1730b7946a79e876bacdb842372433a5cd2230->enter($__internal_dcdf294ab4efbfd2005e9be7fc1730b7946a79e876bacdb842372433a5cd2230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_a0e04b30aee03a8c67e6f8c4a49e42a691772966fe787fab589cc399e37206db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0e04b30aee03a8c67e6f8c4a49e42a691772966fe787fab589cc399e37206db->enter($__internal_a0e04b30aee03a8c67e6f8c4a49e42a691772966fe787fab589cc399e37206db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dcdf294ab4efbfd2005e9be7fc1730b7946a79e876bacdb842372433a5cd2230->leave($__internal_dcdf294ab4efbfd2005e9be7fc1730b7946a79e876bacdb842372433a5cd2230_prof);

        
        $__internal_a0e04b30aee03a8c67e6f8c4a49e42a691772966fe787fab589cc399e37206db->leave($__internal_a0e04b30aee03a8c67e6f8c4a49e42a691772966fe787fab589cc399e37206db_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f0b5d6e2aef51f259800fdae7cd81416e2742cb19bc68f22f35c0c09d785e1d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0b5d6e2aef51f259800fdae7cd81416e2742cb19bc68f22f35c0c09d785e1d9->enter($__internal_f0b5d6e2aef51f259800fdae7cd81416e2742cb19bc68f22f35c0c09d785e1d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9f629656a312e3d19e15e23495c1382cd19ec93f21c7981ea6897896c246664c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f629656a312e3d19e15e23495c1382cd19ec93f21c7981ea6897896c246664c->enter($__internal_9f629656a312e3d19e15e23495c1382cd19ec93f21c7981ea6897896c246664c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9f629656a312e3d19e15e23495c1382cd19ec93f21c7981ea6897896c246664c->leave($__internal_9f629656a312e3d19e15e23495c1382cd19ec93f21c7981ea6897896c246664c_prof);

        
        $__internal_f0b5d6e2aef51f259800fdae7cd81416e2742cb19bc68f22f35c0c09d785e1d9->leave($__internal_f0b5d6e2aef51f259800fdae7cd81416e2742cb19bc68f22f35c0c09d785e1d9_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e5571e7ec82504ce4b3c24541bcc3bf89a55d859761701734bd2cde2a1004f89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5571e7ec82504ce4b3c24541bcc3bf89a55d859761701734bd2cde2a1004f89->enter($__internal_e5571e7ec82504ce4b3c24541bcc3bf89a55d859761701734bd2cde2a1004f89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a1a205b03ad0a28a73a9dda22311a8784d0d98c72bafac77c8c41ada84062164 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1a205b03ad0a28a73a9dda22311a8784d0d98c72bafac77c8c41ada84062164->enter($__internal_a1a205b03ad0a28a73a9dda22311a8784d0d98c72bafac77c8c41ada84062164_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a1a205b03ad0a28a73a9dda22311a8784d0d98c72bafac77c8c41ada84062164->leave($__internal_a1a205b03ad0a28a73a9dda22311a8784d0d98c72bafac77c8c41ada84062164_prof);

        
        $__internal_e5571e7ec82504ce4b3c24541bcc3bf89a55d859761701734bd2cde2a1004f89->leave($__internal_e5571e7ec82504ce4b3c24541bcc3bf89a55d859761701734bd2cde2a1004f89_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9260b993f98e43a915915c3cfde8f50105ef1213f3b51c8aa3c5326e649835be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9260b993f98e43a915915c3cfde8f50105ef1213f3b51c8aa3c5326e649835be->enter($__internal_9260b993f98e43a915915c3cfde8f50105ef1213f3b51c8aa3c5326e649835be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_5703a1cf7257064e3b81148730ae40c424d1fb8cc566287b91e202617b8ae8bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5703a1cf7257064e3b81148730ae40c424d1fb8cc566287b91e202617b8ae8bc->enter($__internal_5703a1cf7257064e3b81148730ae40c424d1fb8cc566287b91e202617b8ae8bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_5703a1cf7257064e3b81148730ae40c424d1fb8cc566287b91e202617b8ae8bc->leave($__internal_5703a1cf7257064e3b81148730ae40c424d1fb8cc566287b91e202617b8ae8bc_prof);

        
        $__internal_9260b993f98e43a915915c3cfde8f50105ef1213f3b51c8aa3c5326e649835be->leave($__internal_9260b993f98e43a915915c3cfde8f50105ef1213f3b51c8aa3c5326e649835be_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
