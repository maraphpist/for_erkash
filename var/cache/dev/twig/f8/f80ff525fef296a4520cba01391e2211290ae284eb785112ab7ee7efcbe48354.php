<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_18776b82c176eb20be163d00ab4ba020495e380c5f58b67ee2829f732ab7e253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bab7d01d717e1962231c0d232c3dd6c2c30f799e91bb70179e50fe44edce37a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bab7d01d717e1962231c0d232c3dd6c2c30f799e91bb70179e50fe44edce37a8->enter($__internal_bab7d01d717e1962231c0d232c3dd6c2c30f799e91bb70179e50fe44edce37a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_a51060c3039c39b58b75c13d84fe2d8487bda517cc5e9f1d99a2297ddde2ba83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a51060c3039c39b58b75c13d84fe2d8487bda517cc5e9f1d99a2297ddde2ba83->enter($__internal_a51060c3039c39b58b75c13d84fe2d8487bda517cc5e9f1d99a2297ddde2ba83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_bab7d01d717e1962231c0d232c3dd6c2c30f799e91bb70179e50fe44edce37a8->leave($__internal_bab7d01d717e1962231c0d232c3dd6c2c30f799e91bb70179e50fe44edce37a8_prof);

        
        $__internal_a51060c3039c39b58b75c13d84fe2d8487bda517cc5e9f1d99a2297ddde2ba83->leave($__internal_a51060c3039c39b58b75c13d84fe2d8487bda517cc5e9f1d99a2297ddde2ba83_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
