<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_35ac850a0417c3a4a8a05e667660d9f613d834031aa28d5e26ef72805f4706a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8595daa886d2616e5edb9573c64651817b78a4328634a15b4f4349d6c960253 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8595daa886d2616e5edb9573c64651817b78a4328634a15b4f4349d6c960253->enter($__internal_c8595daa886d2616e5edb9573c64651817b78a4328634a15b4f4349d6c960253_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_79d549a15884c0fb74d26ba1d098a7e24ce3dc81f556e5deea93fd93dc98bdff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79d549a15884c0fb74d26ba1d098a7e24ce3dc81f556e5deea93fd93dc98bdff->enter($__internal_79d549a15884c0fb74d26ba1d098a7e24ce3dc81f556e5deea93fd93dc98bdff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_c8595daa886d2616e5edb9573c64651817b78a4328634a15b4f4349d6c960253->leave($__internal_c8595daa886d2616e5edb9573c64651817b78a4328634a15b4f4349d6c960253_prof);

        
        $__internal_79d549a15884c0fb74d26ba1d098a7e24ce3dc81f556e5deea93fd93dc98bdff->leave($__internal_79d549a15884c0fb74d26ba1d098a7e24ce3dc81f556e5deea93fd93dc98bdff_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
