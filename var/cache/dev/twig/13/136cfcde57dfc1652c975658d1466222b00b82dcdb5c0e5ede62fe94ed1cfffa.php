<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_8bb5a475bcef4e0db251738308df8e47dbc48a1cd1c7120cdf9a8487afd6b071 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d786a8a0a814dcd7de797c90e46fdfe97ce9e51bb981a4eccced5b02a66ba935 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d786a8a0a814dcd7de797c90e46fdfe97ce9e51bb981a4eccced5b02a66ba935->enter($__internal_d786a8a0a814dcd7de797c90e46fdfe97ce9e51bb981a4eccced5b02a66ba935_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_6464718fb9430f013767a6edd47a227982abc58edb798b4abdf7fd5e40674238 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6464718fb9430f013767a6edd47a227982abc58edb798b4abdf7fd5e40674238->enter($__internal_6464718fb9430f013767a6edd47a227982abc58edb798b4abdf7fd5e40674238_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_d786a8a0a814dcd7de797c90e46fdfe97ce9e51bb981a4eccced5b02a66ba935->leave($__internal_d786a8a0a814dcd7de797c90e46fdfe97ce9e51bb981a4eccced5b02a66ba935_prof);

        
        $__internal_6464718fb9430f013767a6edd47a227982abc58edb798b4abdf7fd5e40674238->leave($__internal_6464718fb9430f013767a6edd47a227982abc58edb798b4abdf7fd5e40674238_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
