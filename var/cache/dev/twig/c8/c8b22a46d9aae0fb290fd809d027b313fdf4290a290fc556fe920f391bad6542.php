<?php

/* base.html.twig */
class __TwigTemplate_47fd51b039e5f4fb7e336b9f260737cefed48cbce97d781ee1d74e5c39c4c062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_338794ca3e5fe5aad824cdcf06a79135406559d0e69a5db200e41b03f5e1ffeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_338794ca3e5fe5aad824cdcf06a79135406559d0e69a5db200e41b03f5e1ffeb->enter($__internal_338794ca3e5fe5aad824cdcf06a79135406559d0e69a5db200e41b03f5e1ffeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_40f435d2cc046fa9d6d5c54147315733ae71bded922eb4b324f000710f6759a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40f435d2cc046fa9d6d5c54147315733ae71bded922eb4b324f000710f6759a3->enter($__internal_40f435d2cc046fa9d6d5c54147315733ae71bded922eb4b324f000710f6759a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_338794ca3e5fe5aad824cdcf06a79135406559d0e69a5db200e41b03f5e1ffeb->leave($__internal_338794ca3e5fe5aad824cdcf06a79135406559d0e69a5db200e41b03f5e1ffeb_prof);

        
        $__internal_40f435d2cc046fa9d6d5c54147315733ae71bded922eb4b324f000710f6759a3->leave($__internal_40f435d2cc046fa9d6d5c54147315733ae71bded922eb4b324f000710f6759a3_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a3139f98f503890af58c319d2266a697b7eed6dd389234d0ff399d794b66a55e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3139f98f503890af58c319d2266a697b7eed6dd389234d0ff399d794b66a55e->enter($__internal_a3139f98f503890af58c319d2266a697b7eed6dd389234d0ff399d794b66a55e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_73db8a9141e1a4048c515caae9c78b0f5cbbfa2fce02576e47a1fe836937a3ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73db8a9141e1a4048c515caae9c78b0f5cbbfa2fce02576e47a1fe836937a3ae->enter($__internal_73db8a9141e1a4048c515caae9c78b0f5cbbfa2fce02576e47a1fe836937a3ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_73db8a9141e1a4048c515caae9c78b0f5cbbfa2fce02576e47a1fe836937a3ae->leave($__internal_73db8a9141e1a4048c515caae9c78b0f5cbbfa2fce02576e47a1fe836937a3ae_prof);

        
        $__internal_a3139f98f503890af58c319d2266a697b7eed6dd389234d0ff399d794b66a55e->leave($__internal_a3139f98f503890af58c319d2266a697b7eed6dd389234d0ff399d794b66a55e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f6e60efab342c32a2dd7f6dd3a6c4bb52418c7b4db85c354acdf4c5dc1c8fee6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6e60efab342c32a2dd7f6dd3a6c4bb52418c7b4db85c354acdf4c5dc1c8fee6->enter($__internal_f6e60efab342c32a2dd7f6dd3a6c4bb52418c7b4db85c354acdf4c5dc1c8fee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_96df9165ac2554708324f06915163f125c12136282fde1c0892adbca2a06c336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96df9165ac2554708324f06915163f125c12136282fde1c0892adbca2a06c336->enter($__internal_96df9165ac2554708324f06915163f125c12136282fde1c0892adbca2a06c336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_96df9165ac2554708324f06915163f125c12136282fde1c0892adbca2a06c336->leave($__internal_96df9165ac2554708324f06915163f125c12136282fde1c0892adbca2a06c336_prof);

        
        $__internal_f6e60efab342c32a2dd7f6dd3a6c4bb52418c7b4db85c354acdf4c5dc1c8fee6->leave($__internal_f6e60efab342c32a2dd7f6dd3a6c4bb52418c7b4db85c354acdf4c5dc1c8fee6_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_9bad119ceda75ef3f775b1da4c4c8e714d678c68f6f9beb210a11256782dfc6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bad119ceda75ef3f775b1da4c4c8e714d678c68f6f9beb210a11256782dfc6f->enter($__internal_9bad119ceda75ef3f775b1da4c4c8e714d678c68f6f9beb210a11256782dfc6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_428fa6aaa63f4980a6d2334be130696fabb4346e550cff372419b63fec4b6bf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_428fa6aaa63f4980a6d2334be130696fabb4346e550cff372419b63fec4b6bf0->enter($__internal_428fa6aaa63f4980a6d2334be130696fabb4346e550cff372419b63fec4b6bf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_428fa6aaa63f4980a6d2334be130696fabb4346e550cff372419b63fec4b6bf0->leave($__internal_428fa6aaa63f4980a6d2334be130696fabb4346e550cff372419b63fec4b6bf0_prof);

        
        $__internal_9bad119ceda75ef3f775b1da4c4c8e714d678c68f6f9beb210a11256782dfc6f->leave($__internal_9bad119ceda75ef3f775b1da4c4c8e714d678c68f6f9beb210a11256782dfc6f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_09b21d15debe44ad6bb2fada565f69da11c0d162c4e73d65aa36a35e0140320e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09b21d15debe44ad6bb2fada565f69da11c0d162c4e73d65aa36a35e0140320e->enter($__internal_09b21d15debe44ad6bb2fada565f69da11c0d162c4e73d65aa36a35e0140320e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d796a964ea1534c4406a6ec2edf422d088705d2348a1c54632bd624a7ac2c17f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d796a964ea1534c4406a6ec2edf422d088705d2348a1c54632bd624a7ac2c17f->enter($__internal_d796a964ea1534c4406a6ec2edf422d088705d2348a1c54632bd624a7ac2c17f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d796a964ea1534c4406a6ec2edf422d088705d2348a1c54632bd624a7ac2c17f->leave($__internal_d796a964ea1534c4406a6ec2edf422d088705d2348a1c54632bd624a7ac2c17f_prof);

        
        $__internal_09b21d15debe44ad6bb2fada565f69da11c0d162c4e73d65aa36a35e0140320e->leave($__internal_09b21d15debe44ad6bb2fada565f69da11c0d162c4e73d65aa36a35e0140320e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/app/Resources/views/base.html.twig");
    }
}
