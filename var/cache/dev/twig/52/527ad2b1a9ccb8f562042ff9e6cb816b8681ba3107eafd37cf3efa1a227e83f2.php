<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_624075785c095260ceab89457b4dac6a48a241898921aeb6cd8257a204ee9b36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_747980e3d956378f1b891f14719d6a7945b158d067c278da22ba471550e4409b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_747980e3d956378f1b891f14719d6a7945b158d067c278da22ba471550e4409b->enter($__internal_747980e3d956378f1b891f14719d6a7945b158d067c278da22ba471550e4409b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_cc1e25a3d4d790544f945f507aaa1691fe1a8ceedf8f3d520f35c56bf9b79091 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc1e25a3d4d790544f945f507aaa1691fe1a8ceedf8f3d520f35c56bf9b79091->enter($__internal_cc1e25a3d4d790544f945f507aaa1691fe1a8ceedf8f3d520f35c56bf9b79091_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_747980e3d956378f1b891f14719d6a7945b158d067c278da22ba471550e4409b->leave($__internal_747980e3d956378f1b891f14719d6a7945b158d067c278da22ba471550e4409b_prof);

        
        $__internal_cc1e25a3d4d790544f945f507aaa1691fe1a8ceedf8f3d520f35c56bf9b79091->leave($__internal_cc1e25a3d4d790544f945f507aaa1691fe1a8ceedf8f3d520f35c56bf9b79091_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
