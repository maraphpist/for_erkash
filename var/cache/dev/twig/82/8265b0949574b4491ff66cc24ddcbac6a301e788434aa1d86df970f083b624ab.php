<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_d1c001d7854d4dc42289f0d0554a79f785bb8597d75766d5187c38da7138059c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_88be40643084ba1702b8a7902b7a5a99bbe6360505bcd937f80c6e2aa4e39df9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88be40643084ba1702b8a7902b7a5a99bbe6360505bcd937f80c6e2aa4e39df9->enter($__internal_88be40643084ba1702b8a7902b7a5a99bbe6360505bcd937f80c6e2aa4e39df9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_3fb9fad77a71c558fa870c37bd87a93a2ccd1897445d3a853653fe15d82edc13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fb9fad77a71c558fa870c37bd87a93a2ccd1897445d3a853653fe15d82edc13->enter($__internal_3fb9fad77a71c558fa870c37bd87a93a2ccd1897445d3a853653fe15d82edc13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_88be40643084ba1702b8a7902b7a5a99bbe6360505bcd937f80c6e2aa4e39df9->leave($__internal_88be40643084ba1702b8a7902b7a5a99bbe6360505bcd937f80c6e2aa4e39df9_prof);

        
        $__internal_3fb9fad77a71c558fa870c37bd87a93a2ccd1897445d3a853653fe15d82edc13->leave($__internal_3fb9fad77a71c558fa870c37bd87a93a2ccd1897445d3a853653fe15d82edc13_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
