<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_565d7b34017e7b0d00cabe1ad15d79198e7f69bc178b026aab416bff8ead0d57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ade7c5e5c3dc27c081c34be6c4ece3d38c43bcd107ee5b52329189269acec1e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ade7c5e5c3dc27c081c34be6c4ece3d38c43bcd107ee5b52329189269acec1e8->enter($__internal_ade7c5e5c3dc27c081c34be6c4ece3d38c43bcd107ee5b52329189269acec1e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_fcae7fd617d66906bb8e9ea100bda53bd2e6c96fdd67c030fe4b9c61ae1e8911 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcae7fd617d66906bb8e9ea100bda53bd2e6c96fdd67c030fe4b9c61ae1e8911->enter($__internal_fcae7fd617d66906bb8e9ea100bda53bd2e6c96fdd67c030fe4b9c61ae1e8911_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_ade7c5e5c3dc27c081c34be6c4ece3d38c43bcd107ee5b52329189269acec1e8->leave($__internal_ade7c5e5c3dc27c081c34be6c4ece3d38c43bcd107ee5b52329189269acec1e8_prof);

        
        $__internal_fcae7fd617d66906bb8e9ea100bda53bd2e6c96fdd67c030fe4b9c61ae1e8911->leave($__internal_fcae7fd617d66906bb8e9ea100bda53bd2e6c96fdd67c030fe4b9c61ae1e8911_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
