<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_04b7e365edc6d0270359b108e26ab84aed7e2c8714c81d85055c1b56c5fbfa66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13af93013efe07336b560ca0a7a19ca4974027af6ae6f0bfee6abb42760fa7d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13af93013efe07336b560ca0a7a19ca4974027af6ae6f0bfee6abb42760fa7d9->enter($__internal_13af93013efe07336b560ca0a7a19ca4974027af6ae6f0bfee6abb42760fa7d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_f26f5a5c0e23a5327a719e416ee7ab55c7987914b44b14cba7b2cf2290f74725 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f26f5a5c0e23a5327a719e416ee7ab55c7987914b44b14cba7b2cf2290f74725->enter($__internal_f26f5a5c0e23a5327a719e416ee7ab55c7987914b44b14cba7b2cf2290f74725_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_13af93013efe07336b560ca0a7a19ca4974027af6ae6f0bfee6abb42760fa7d9->leave($__internal_13af93013efe07336b560ca0a7a19ca4974027af6ae6f0bfee6abb42760fa7d9_prof);

        
        $__internal_f26f5a5c0e23a5327a719e416ee7ab55c7987914b44b14cba7b2cf2290f74725->leave($__internal_f26f5a5c0e23a5327a719e416ee7ab55c7987914b44b14cba7b2cf2290f74725_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_00a648fd13bff7bf64f98ad79d972ab640106ae2788cde68cf0c3d012015403f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00a648fd13bff7bf64f98ad79d972ab640106ae2788cde68cf0c3d012015403f->enter($__internal_00a648fd13bff7bf64f98ad79d972ab640106ae2788cde68cf0c3d012015403f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_83d0b6781772dcd7cf37c89f23c95bbe41b26727cceaf8c56a5357002117dec4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83d0b6781772dcd7cf37c89f23c95bbe41b26727cceaf8c56a5357002117dec4->enter($__internal_83d0b6781772dcd7cf37c89f23c95bbe41b26727cceaf8c56a5357002117dec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_83d0b6781772dcd7cf37c89f23c95bbe41b26727cceaf8c56a5357002117dec4->leave($__internal_83d0b6781772dcd7cf37c89f23c95bbe41b26727cceaf8c56a5357002117dec4_prof);

        
        $__internal_00a648fd13bff7bf64f98ad79d972ab640106ae2788cde68cf0c3d012015403f->leave($__internal_00a648fd13bff7bf64f98ad79d972ab640106ae2788cde68cf0c3d012015403f_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_a16067aac68c935d34cd59b1caec6689046f1160fc89e6859cb20e5a447924cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a16067aac68c935d34cd59b1caec6689046f1160fc89e6859cb20e5a447924cc->enter($__internal_a16067aac68c935d34cd59b1caec6689046f1160fc89e6859cb20e5a447924cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_830c945e8061beca92fd6dd8f7f860d97fc51d0f282a9f8e4b51c3c6605af09d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_830c945e8061beca92fd6dd8f7f860d97fc51d0f282a9f8e4b51c3c6605af09d->enter($__internal_830c945e8061beca92fd6dd8f7f860d97fc51d0f282a9f8e4b51c3c6605af09d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_830c945e8061beca92fd6dd8f7f860d97fc51d0f282a9f8e4b51c3c6605af09d->leave($__internal_830c945e8061beca92fd6dd8f7f860d97fc51d0f282a9f8e4b51c3c6605af09d_prof);

        
        $__internal_a16067aac68c935d34cd59b1caec6689046f1160fc89e6859cb20e5a447924cc->leave($__internal_a16067aac68c935d34cd59b1caec6689046f1160fc89e6859cb20e5a447924cc_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
