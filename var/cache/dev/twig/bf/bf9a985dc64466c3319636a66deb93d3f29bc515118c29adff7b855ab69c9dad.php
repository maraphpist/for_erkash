<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_6411630e49d7556c6b402a950d82b722ad3ab8c454cda3acc41d70aeb16697a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bed2619f77e6858677bdafbae0555e23b0c1be4a3015117a74ca28b148123cf0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bed2619f77e6858677bdafbae0555e23b0c1be4a3015117a74ca28b148123cf0->enter($__internal_bed2619f77e6858677bdafbae0555e23b0c1be4a3015117a74ca28b148123cf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_ce5231b27e7aae68a6c941a9467c13ef3386f1b58b7d97c95498c4a9916d4847 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce5231b27e7aae68a6c941a9467c13ef3386f1b58b7d97c95498c4a9916d4847->enter($__internal_ce5231b27e7aae68a6c941a9467c13ef3386f1b58b7d97c95498c4a9916d4847_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_bed2619f77e6858677bdafbae0555e23b0c1be4a3015117a74ca28b148123cf0->leave($__internal_bed2619f77e6858677bdafbae0555e23b0c1be4a3015117a74ca28b148123cf0_prof);

        
        $__internal_ce5231b27e7aae68a6c941a9467c13ef3386f1b58b7d97c95498c4a9916d4847->leave($__internal_ce5231b27e7aae68a6c941a9467c13ef3386f1b58b7d97c95498c4a9916d4847_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
