<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_77cb403ac9f5d693785a8bf26efa5d431e95dcc39820057b02a197d57f751ce0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6417529825323873b91a9a4614bd482778d939dbb41058af2aa6d3d729e2b86d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6417529825323873b91a9a4614bd482778d939dbb41058af2aa6d3d729e2b86d->enter($__internal_6417529825323873b91a9a4614bd482778d939dbb41058af2aa6d3d729e2b86d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_db5bf8d1db52df4e53b576cdf8e0c10e58ce03d11b28b92429c0291842cfa8a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db5bf8d1db52df4e53b576cdf8e0c10e58ce03d11b28b92429c0291842cfa8a8->enter($__internal_db5bf8d1db52df4e53b576cdf8e0c10e58ce03d11b28b92429c0291842cfa8a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_6417529825323873b91a9a4614bd482778d939dbb41058af2aa6d3d729e2b86d->leave($__internal_6417529825323873b91a9a4614bd482778d939dbb41058af2aa6d3d729e2b86d_prof);

        
        $__internal_db5bf8d1db52df4e53b576cdf8e0c10e58ce03d11b28b92429c0291842cfa8a8->leave($__internal_db5bf8d1db52df4e53b576cdf8e0c10e58ce03d11b28b92429c0291842cfa8a8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
