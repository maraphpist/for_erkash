<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_7bd36af570d7e8e3cd6332c5b810750aae9bc5356b7a1fc7226b578b8debba57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24027f76eef6e636310168d6ca59edb729f90e3d52ec6abf3ad52575ebb3fa07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24027f76eef6e636310168d6ca59edb729f90e3d52ec6abf3ad52575ebb3fa07->enter($__internal_24027f76eef6e636310168d6ca59edb729f90e3d52ec6abf3ad52575ebb3fa07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_1f71cdbeb06a17949bb44f2840b908cb185da46535f2e19d7603873c6281c446 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f71cdbeb06a17949bb44f2840b908cb185da46535f2e19d7603873c6281c446->enter($__internal_1f71cdbeb06a17949bb44f2840b908cb185da46535f2e19d7603873c6281c446_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_24027f76eef6e636310168d6ca59edb729f90e3d52ec6abf3ad52575ebb3fa07->leave($__internal_24027f76eef6e636310168d6ca59edb729f90e3d52ec6abf3ad52575ebb3fa07_prof);

        
        $__internal_1f71cdbeb06a17949bb44f2840b908cb185da46535f2e19d7603873c6281c446->leave($__internal_1f71cdbeb06a17949bb44f2840b908cb185da46535f2e19d7603873c6281c446_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
