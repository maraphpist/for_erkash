<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_8f797635c6560fcf0931d89d6d05cc91c8d4cbbe7d51be4498a2d1830255611d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd426392e598f09f4906ffae02d0361265806718a9002a01e3ab54749940bff4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd426392e598f09f4906ffae02d0361265806718a9002a01e3ab54749940bff4->enter($__internal_dd426392e598f09f4906ffae02d0361265806718a9002a01e3ab54749940bff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_ff0425f441bbf1f933c611fe7c40acb092b63e140421694c2c5c27528c5f69b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff0425f441bbf1f933c611fe7c40acb092b63e140421694c2c5c27528c5f69b9->enter($__internal_ff0425f441bbf1f933c611fe7c40acb092b63e140421694c2c5c27528c5f69b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd426392e598f09f4906ffae02d0361265806718a9002a01e3ab54749940bff4->leave($__internal_dd426392e598f09f4906ffae02d0361265806718a9002a01e3ab54749940bff4_prof);

        
        $__internal_ff0425f441bbf1f933c611fe7c40acb092b63e140421694c2c5c27528c5f69b9->leave($__internal_ff0425f441bbf1f933c611fe7c40acb092b63e140421694c2c5c27528c5f69b9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3aaea60e39ce41478961d2e4d50b3e767ce6c58c9e3c233d399ba58a2d0d201a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3aaea60e39ce41478961d2e4d50b3e767ce6c58c9e3c233d399ba58a2d0d201a->enter($__internal_3aaea60e39ce41478961d2e4d50b3e767ce6c58c9e3c233d399ba58a2d0d201a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_519b9e8fe9d3c9a91bd403039318037db00c1aacedada91fc9aa730ecf1407fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_519b9e8fe9d3c9a91bd403039318037db00c1aacedada91fc9aa730ecf1407fe->enter($__internal_519b9e8fe9d3c9a91bd403039318037db00c1aacedada91fc9aa730ecf1407fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_519b9e8fe9d3c9a91bd403039318037db00c1aacedada91fc9aa730ecf1407fe->leave($__internal_519b9e8fe9d3c9a91bd403039318037db00c1aacedada91fc9aa730ecf1407fe_prof);

        
        $__internal_3aaea60e39ce41478961d2e4d50b3e767ce6c58c9e3c233d399ba58a2d0d201a->leave($__internal_3aaea60e39ce41478961d2e4d50b3e767ce6c58c9e3c233d399ba58a2d0d201a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
