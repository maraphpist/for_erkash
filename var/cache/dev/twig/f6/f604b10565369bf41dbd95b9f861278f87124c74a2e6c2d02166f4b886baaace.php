<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_94f2eb4198013dc295c045882ffcee0c845e89a9371ac29f4ad50235d639f4ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc7471fc4b5bb08853dd9f615337dbaa9ca33e6ebb656d53456afed78301c284 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc7471fc4b5bb08853dd9f615337dbaa9ca33e6ebb656d53456afed78301c284->enter($__internal_dc7471fc4b5bb08853dd9f615337dbaa9ca33e6ebb656d53456afed78301c284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_fee208c96109faf0a5a2e3a522f32421f94b97cf244e156bf40dfe89b17985fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fee208c96109faf0a5a2e3a522f32421f94b97cf244e156bf40dfe89b17985fd->enter($__internal_fee208c96109faf0a5a2e3a522f32421f94b97cf244e156bf40dfe89b17985fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_dc7471fc4b5bb08853dd9f615337dbaa9ca33e6ebb656d53456afed78301c284->leave($__internal_dc7471fc4b5bb08853dd9f615337dbaa9ca33e6ebb656d53456afed78301c284_prof);

        
        $__internal_fee208c96109faf0a5a2e3a522f32421f94b97cf244e156bf40dfe89b17985fd->leave($__internal_fee208c96109faf0a5a2e3a522f32421f94b97cf244e156bf40dfe89b17985fd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
