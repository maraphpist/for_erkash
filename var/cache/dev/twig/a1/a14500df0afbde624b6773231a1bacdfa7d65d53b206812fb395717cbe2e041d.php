<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_bc4ea1beb28828ce335227ee50b2e3d5971fbb5a697333240a240d7c558d8474 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c23d02b51d9098beef939f42139f8d8a32f6b749b0a211648138dccecc7c94e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c23d02b51d9098beef939f42139f8d8a32f6b749b0a211648138dccecc7c94e->enter($__internal_0c23d02b51d9098beef939f42139f8d8a32f6b749b0a211648138dccecc7c94e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_c2e6ea4f0c0e09dca11b387aa4b956a2d56e9d9bd60147c0ac7a2d67f0026fbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2e6ea4f0c0e09dca11b387aa4b956a2d56e9d9bd60147c0ac7a2d67f0026fbe->enter($__internal_c2e6ea4f0c0e09dca11b387aa4b956a2d56e9d9bd60147c0ac7a2d67f0026fbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0c23d02b51d9098beef939f42139f8d8a32f6b749b0a211648138dccecc7c94e->leave($__internal_0c23d02b51d9098beef939f42139f8d8a32f6b749b0a211648138dccecc7c94e_prof);

        
        $__internal_c2e6ea4f0c0e09dca11b387aa4b956a2d56e9d9bd60147c0ac7a2d67f0026fbe->leave($__internal_c2e6ea4f0c0e09dca11b387aa4b956a2d56e9d9bd60147c0ac7a2d67f0026fbe_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_90c11d8dd934ba2909feb94e89147db65012903a4641782bd360b50413b5c0dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90c11d8dd934ba2909feb94e89147db65012903a4641782bd360b50413b5c0dc->enter($__internal_90c11d8dd934ba2909feb94e89147db65012903a4641782bd360b50413b5c0dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_eacc92e1bad609a77eba5e5e8d17fb3cdcb2e2f3c7f40c422c36fb6f1d5b8aa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eacc92e1bad609a77eba5e5e8d17fb3cdcb2e2f3c7f40c422c36fb6f1d5b8aa7->enter($__internal_eacc92e1bad609a77eba5e5e8d17fb3cdcb2e2f3c7f40c422c36fb6f1d5b8aa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_eacc92e1bad609a77eba5e5e8d17fb3cdcb2e2f3c7f40c422c36fb6f1d5b8aa7->leave($__internal_eacc92e1bad609a77eba5e5e8d17fb3cdcb2e2f3c7f40c422c36fb6f1d5b8aa7_prof);

        
        $__internal_90c11d8dd934ba2909feb94e89147db65012903a4641782bd360b50413b5c0dc->leave($__internal_90c11d8dd934ba2909feb94e89147db65012903a4641782bd360b50413b5c0dc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
