<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_f3ed18cf424b56f95d3e2b09d1f7ff84c2cf2d9dcde67f8574d093c3bac364ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07fb74186bb36af40269915af424ae96684e067069d1137841e6b96803414f00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07fb74186bb36af40269915af424ae96684e067069d1137841e6b96803414f00->enter($__internal_07fb74186bb36af40269915af424ae96684e067069d1137841e6b96803414f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_6816114f5589573e3dfbd35b5a20e33702d10b00cc9933bc9342aff17ab93d29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6816114f5589573e3dfbd35b5a20e33702d10b00cc9933bc9342aff17ab93d29->enter($__internal_6816114f5589573e3dfbd35b5a20e33702d10b00cc9933bc9342aff17ab93d29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_07fb74186bb36af40269915af424ae96684e067069d1137841e6b96803414f00->leave($__internal_07fb74186bb36af40269915af424ae96684e067069d1137841e6b96803414f00_prof);

        
        $__internal_6816114f5589573e3dfbd35b5a20e33702d10b00cc9933bc9342aff17ab93d29->leave($__internal_6816114f5589573e3dfbd35b5a20e33702d10b00cc9933bc9342aff17ab93d29_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
