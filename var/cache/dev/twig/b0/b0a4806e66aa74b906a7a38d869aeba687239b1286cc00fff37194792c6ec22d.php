<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_89612bd6c62318aa241fa727dd099b5497e535db6ead2ad52446c47f14020461 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60fe0cd9820c2303bb90a723121f86c0d9bd4602550578ca3584827e829c872d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60fe0cd9820c2303bb90a723121f86c0d9bd4602550578ca3584827e829c872d->enter($__internal_60fe0cd9820c2303bb90a723121f86c0d9bd4602550578ca3584827e829c872d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_215d1e807158c9e9aa60562eb96574472f9a2d7c146d4314b62d3b60f586ad2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_215d1e807158c9e9aa60562eb96574472f9a2d7c146d4314b62d3b60f586ad2d->enter($__internal_215d1e807158c9e9aa60562eb96574472f9a2d7c146d4314b62d3b60f586ad2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_60fe0cd9820c2303bb90a723121f86c0d9bd4602550578ca3584827e829c872d->leave($__internal_60fe0cd9820c2303bb90a723121f86c0d9bd4602550578ca3584827e829c872d_prof);

        
        $__internal_215d1e807158c9e9aa60562eb96574472f9a2d7c146d4314b62d3b60f586ad2d->leave($__internal_215d1e807158c9e9aa60562eb96574472f9a2d7c146d4314b62d3b60f586ad2d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
