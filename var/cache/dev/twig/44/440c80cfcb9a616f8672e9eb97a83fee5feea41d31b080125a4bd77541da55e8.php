<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_aa31c7e191ff782a3a5468e162637150d42504414a35e2c448f799d93d4b86d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a569154311dc728c7cfce2d09169be1b51444cb4f0a8137218fcd9aada7eba7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a569154311dc728c7cfce2d09169be1b51444cb4f0a8137218fcd9aada7eba7->enter($__internal_2a569154311dc728c7cfce2d09169be1b51444cb4f0a8137218fcd9aada7eba7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_c433ab3ef4a43163bd3089a29b8b4ef9c534f8960b1e8058927fdc53625a0daa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c433ab3ef4a43163bd3089a29b8b4ef9c534f8960b1e8058927fdc53625a0daa->enter($__internal_c433ab3ef4a43163bd3089a29b8b4ef9c534f8960b1e8058927fdc53625a0daa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_2a569154311dc728c7cfce2d09169be1b51444cb4f0a8137218fcd9aada7eba7->leave($__internal_2a569154311dc728c7cfce2d09169be1b51444cb4f0a8137218fcd9aada7eba7_prof);

        
        $__internal_c433ab3ef4a43163bd3089a29b8b4ef9c534f8960b1e8058927fdc53625a0daa->leave($__internal_c433ab3ef4a43163bd3089a29b8b4ef9c534f8960b1e8058927fdc53625a0daa_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
