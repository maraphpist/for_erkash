<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_37e46c76d02729b1ef2fea2576c1b1aba23cda9eef4c1c20a78c979265363716 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16d38f0bba40c54ffe3421db1f212b01da974e7f085000ec3110441dd7a4b03e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16d38f0bba40c54ffe3421db1f212b01da974e7f085000ec3110441dd7a4b03e->enter($__internal_16d38f0bba40c54ffe3421db1f212b01da974e7f085000ec3110441dd7a4b03e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_014e617dd80a846692b8d40c6b3e079fc4a29a009a24438c50afd27d669a0b0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_014e617dd80a846692b8d40c6b3e079fc4a29a009a24438c50afd27d669a0b0d->enter($__internal_014e617dd80a846692b8d40c6b3e079fc4a29a009a24438c50afd27d669a0b0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_16d38f0bba40c54ffe3421db1f212b01da974e7f085000ec3110441dd7a4b03e->leave($__internal_16d38f0bba40c54ffe3421db1f212b01da974e7f085000ec3110441dd7a4b03e_prof);

        
        $__internal_014e617dd80a846692b8d40c6b3e079fc4a29a009a24438c50afd27d669a0b0d->leave($__internal_014e617dd80a846692b8d40c6b3e079fc4a29a009a24438c50afd27d669a0b0d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
