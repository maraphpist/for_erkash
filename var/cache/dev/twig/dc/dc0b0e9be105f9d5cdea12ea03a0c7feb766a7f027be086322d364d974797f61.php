<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_acacba1794e10f6c9f681ada3edf5afef455597ac8d2cf1b0f35f2e08cb37226 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64f2323d0727bda1bae6022cf4d21d929210e1db82dadbe54574cf31d87e7e1c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64f2323d0727bda1bae6022cf4d21d929210e1db82dadbe54574cf31d87e7e1c->enter($__internal_64f2323d0727bda1bae6022cf4d21d929210e1db82dadbe54574cf31d87e7e1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_e9c816a31d68aa0b33dffa5cc8a10ec4874e02877580693970fbbcef7e39c10f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9c816a31d68aa0b33dffa5cc8a10ec4874e02877580693970fbbcef7e39c10f->enter($__internal_e9c816a31d68aa0b33dffa5cc8a10ec4874e02877580693970fbbcef7e39c10f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_64f2323d0727bda1bae6022cf4d21d929210e1db82dadbe54574cf31d87e7e1c->leave($__internal_64f2323d0727bda1bae6022cf4d21d929210e1db82dadbe54574cf31d87e7e1c_prof);

        
        $__internal_e9c816a31d68aa0b33dffa5cc8a10ec4874e02877580693970fbbcef7e39c10f->leave($__internal_e9c816a31d68aa0b33dffa5cc8a10ec4874e02877580693970fbbcef7e39c10f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
