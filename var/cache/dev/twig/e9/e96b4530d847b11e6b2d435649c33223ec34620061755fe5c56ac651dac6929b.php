<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_bf1e9f33d8870d9f7194eb22f9bd0f79ed8c8337b2955d3761d55338cfa86369 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c402ec738e8e826b9b8834ec637b0daaaa187f88d63c10114eaa4f4ca384cc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c402ec738e8e826b9b8834ec637b0daaaa187f88d63c10114eaa4f4ca384cc1->enter($__internal_4c402ec738e8e826b9b8834ec637b0daaaa187f88d63c10114eaa4f4ca384cc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_b4037c116b1d9dffe577a77c6d309d0102bc6a6aa306e99fe2f168c6b104020b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4037c116b1d9dffe577a77c6d309d0102bc6a6aa306e99fe2f168c6b104020b->enter($__internal_b4037c116b1d9dffe577a77c6d309d0102bc6a6aa306e99fe2f168c6b104020b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_4c402ec738e8e826b9b8834ec637b0daaaa187f88d63c10114eaa4f4ca384cc1->leave($__internal_4c402ec738e8e826b9b8834ec637b0daaaa187f88d63c10114eaa4f4ca384cc1_prof);

        
        $__internal_b4037c116b1d9dffe577a77c6d309d0102bc6a6aa306e99fe2f168c6b104020b->leave($__internal_b4037c116b1d9dffe577a77c6d309d0102bc6a6aa306e99fe2f168c6b104020b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
