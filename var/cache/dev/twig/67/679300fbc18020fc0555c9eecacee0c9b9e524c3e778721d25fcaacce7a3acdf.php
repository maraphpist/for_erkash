<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_8164888d581e84e2c2e32d1d7e2cc60682776e22eade1d87e4afcf2377b3459d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66a6309a009e6e4eaef3d529e501fc6f1184527061639d3b1662fe970923efbe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66a6309a009e6e4eaef3d529e501fc6f1184527061639d3b1662fe970923efbe->enter($__internal_66a6309a009e6e4eaef3d529e501fc6f1184527061639d3b1662fe970923efbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_e5c927851cca17cd1104f114d167eb2850d7613e72e4f0c2ceb6120e4fb0609f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5c927851cca17cd1104f114d167eb2850d7613e72e4f0c2ceb6120e4fb0609f->enter($__internal_e5c927851cca17cd1104f114d167eb2850d7613e72e4f0c2ceb6120e4fb0609f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_66a6309a009e6e4eaef3d529e501fc6f1184527061639d3b1662fe970923efbe->leave($__internal_66a6309a009e6e4eaef3d529e501fc6f1184527061639d3b1662fe970923efbe_prof);

        
        $__internal_e5c927851cca17cd1104f114d167eb2850d7613e72e4f0c2ceb6120e4fb0609f->leave($__internal_e5c927851cca17cd1104f114d167eb2850d7613e72e4f0c2ceb6120e4fb0609f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
