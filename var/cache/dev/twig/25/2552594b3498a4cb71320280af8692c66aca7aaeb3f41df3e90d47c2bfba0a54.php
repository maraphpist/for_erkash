<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_5822d96dc649f46e30dff249a3a71de6a19ae062cb782a290b55e829dac1467e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7a8f7eedad4eb2447b81f28f124fef4dd15aa6211577dc568c7424c67ef2301 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7a8f7eedad4eb2447b81f28f124fef4dd15aa6211577dc568c7424c67ef2301->enter($__internal_b7a8f7eedad4eb2447b81f28f124fef4dd15aa6211577dc568c7424c67ef2301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_e5ba4f17185922fa301a0e6245056abebf342feb45902af1248a354bdb725334 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5ba4f17185922fa301a0e6245056abebf342feb45902af1248a354bdb725334->enter($__internal_e5ba4f17185922fa301a0e6245056abebf342feb45902af1248a354bdb725334_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_b7a8f7eedad4eb2447b81f28f124fef4dd15aa6211577dc568c7424c67ef2301->leave($__internal_b7a8f7eedad4eb2447b81f28f124fef4dd15aa6211577dc568c7424c67ef2301_prof);

        
        $__internal_e5ba4f17185922fa301a0e6245056abebf342feb45902af1248a354bdb725334->leave($__internal_e5ba4f17185922fa301a0e6245056abebf342feb45902af1248a354bdb725334_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_2b404f308ea332ce9d769dba0de88cdaa8b8c6b09236e893fbe702faf816cd71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b404f308ea332ce9d769dba0de88cdaa8b8c6b09236e893fbe702faf816cd71->enter($__internal_2b404f308ea332ce9d769dba0de88cdaa8b8c6b09236e893fbe702faf816cd71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_da6e1433013ce1ff3b7b4362f54d046e6f3236f6906feee5da99f8858e91d4fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da6e1433013ce1ff3b7b4362f54d046e6f3236f6906feee5da99f8858e91d4fb->enter($__internal_da6e1433013ce1ff3b7b4362f54d046e6f3236f6906feee5da99f8858e91d4fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_da6e1433013ce1ff3b7b4362f54d046e6f3236f6906feee5da99f8858e91d4fb->leave($__internal_da6e1433013ce1ff3b7b4362f54d046e6f3236f6906feee5da99f8858e91d4fb_prof);

        
        $__internal_2b404f308ea332ce9d769dba0de88cdaa8b8c6b09236e893fbe702faf816cd71->leave($__internal_2b404f308ea332ce9d769dba0de88cdaa8b8c6b09236e893fbe702faf816cd71_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_099b8a6e5f40c843b4c0246a4fef2154ebc971c8205f624d774d621fc25813a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_099b8a6e5f40c843b4c0246a4fef2154ebc971c8205f624d774d621fc25813a7->enter($__internal_099b8a6e5f40c843b4c0246a4fef2154ebc971c8205f624d774d621fc25813a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_58aa4fb501d2848335fc8e8ac28f1ff30f9d7eaf889994690e499e7123f9aa41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58aa4fb501d2848335fc8e8ac28f1ff30f9d7eaf889994690e499e7123f9aa41->enter($__internal_58aa4fb501d2848335fc8e8ac28f1ff30f9d7eaf889994690e499e7123f9aa41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_58aa4fb501d2848335fc8e8ac28f1ff30f9d7eaf889994690e499e7123f9aa41->leave($__internal_58aa4fb501d2848335fc8e8ac28f1ff30f9d7eaf889994690e499e7123f9aa41_prof);

        
        $__internal_099b8a6e5f40c843b4c0246a4fef2154ebc971c8205f624d774d621fc25813a7->leave($__internal_099b8a6e5f40c843b4c0246a4fef2154ebc971c8205f624d774d621fc25813a7_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_d28382e45ac7c88da0f0bbd4cd035c9d39764172fb9f7b363816f332b54295e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d28382e45ac7c88da0f0bbd4cd035c9d39764172fb9f7b363816f332b54295e6->enter($__internal_d28382e45ac7c88da0f0bbd4cd035c9d39764172fb9f7b363816f332b54295e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c78b57c6500d766787628c3f2b39065b74a423593ad5f9de77c6df472419796b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c78b57c6500d766787628c3f2b39065b74a423593ad5f9de77c6df472419796b->enter($__internal_c78b57c6500d766787628c3f2b39065b74a423593ad5f9de77c6df472419796b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c78b57c6500d766787628c3f2b39065b74a423593ad5f9de77c6df472419796b->leave($__internal_c78b57c6500d766787628c3f2b39065b74a423593ad5f9de77c6df472419796b_prof);

        
        $__internal_d28382e45ac7c88da0f0bbd4cd035c9d39764172fb9f7b363816f332b54295e6->leave($__internal_d28382e45ac7c88da0f0bbd4cd035c9d39764172fb9f7b363816f332b54295e6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
