<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_11e5710c6af86f6f6ce133a04312d2493e47fd7fe737533a5296fb25c92db98b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9883cc379ccda1495c6b1ffe231ae7860ddedb2a153ea57f0ecd8ce0a07496de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9883cc379ccda1495c6b1ffe231ae7860ddedb2a153ea57f0ecd8ce0a07496de->enter($__internal_9883cc379ccda1495c6b1ffe231ae7860ddedb2a153ea57f0ecd8ce0a07496de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_5a04f713211edf0432fd05505c4081604d5cf1940ed75aa1b45ec202a6c07301 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a04f713211edf0432fd05505c4081604d5cf1940ed75aa1b45ec202a6c07301->enter($__internal_5a04f713211edf0432fd05505c4081604d5cf1940ed75aa1b45ec202a6c07301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_9883cc379ccda1495c6b1ffe231ae7860ddedb2a153ea57f0ecd8ce0a07496de->leave($__internal_9883cc379ccda1495c6b1ffe231ae7860ddedb2a153ea57f0ecd8ce0a07496de_prof);

        
        $__internal_5a04f713211edf0432fd05505c4081604d5cf1940ed75aa1b45ec202a6c07301->leave($__internal_5a04f713211edf0432fd05505c4081604d5cf1940ed75aa1b45ec202a6c07301_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
