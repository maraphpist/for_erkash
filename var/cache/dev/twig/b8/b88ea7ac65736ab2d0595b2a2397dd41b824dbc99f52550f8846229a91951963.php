<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_69e6d9723c4d7a5da85fb4917391785023e8450ec8c33c1d8eabb645a9fcee6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ec16ec3a9546fb8575085fcd1eb82b58e083f9d580afe37c76cfdb77c3e1359 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ec16ec3a9546fb8575085fcd1eb82b58e083f9d580afe37c76cfdb77c3e1359->enter($__internal_5ec16ec3a9546fb8575085fcd1eb82b58e083f9d580afe37c76cfdb77c3e1359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_8eb4f25b2fe91907bdd14ff3f63c0c101537cd91cdc2a0e7de5798182d0f8afa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8eb4f25b2fe91907bdd14ff3f63c0c101537cd91cdc2a0e7de5798182d0f8afa->enter($__internal_8eb4f25b2fe91907bdd14ff3f63c0c101537cd91cdc2a0e7de5798182d0f8afa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ec16ec3a9546fb8575085fcd1eb82b58e083f9d580afe37c76cfdb77c3e1359->leave($__internal_5ec16ec3a9546fb8575085fcd1eb82b58e083f9d580afe37c76cfdb77c3e1359_prof);

        
        $__internal_8eb4f25b2fe91907bdd14ff3f63c0c101537cd91cdc2a0e7de5798182d0f8afa->leave($__internal_8eb4f25b2fe91907bdd14ff3f63c0c101537cd91cdc2a0e7de5798182d0f8afa_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5278f66346930e64195a5f0145559ef0c2a8f32cb6d10396ac94feadf4ead255 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5278f66346930e64195a5f0145559ef0c2a8f32cb6d10396ac94feadf4ead255->enter($__internal_5278f66346930e64195a5f0145559ef0c2a8f32cb6d10396ac94feadf4ead255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b4ede67048ef4b973739e0a4837e0b73880389f11683dbe27b8d300d2dbfde3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4ede67048ef4b973739e0a4837e0b73880389f11683dbe27b8d300d2dbfde3b->enter($__internal_b4ede67048ef4b973739e0a4837e0b73880389f11683dbe27b8d300d2dbfde3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_b4ede67048ef4b973739e0a4837e0b73880389f11683dbe27b8d300d2dbfde3b->leave($__internal_b4ede67048ef4b973739e0a4837e0b73880389f11683dbe27b8d300d2dbfde3b_prof);

        
        $__internal_5278f66346930e64195a5f0145559ef0c2a8f32cb6d10396ac94feadf4ead255->leave($__internal_5278f66346930e64195a5f0145559ef0c2a8f32cb6d10396ac94feadf4ead255_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
