<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_2513e19d2d13d7bbf5d6f912b3544f49838f36e56415939132eddc3ef29bc2ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0116ea0d0316ed189d7be1e467dc3b79ba1973b017410820e3342f67ffb2fa77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0116ea0d0316ed189d7be1e467dc3b79ba1973b017410820e3342f67ffb2fa77->enter($__internal_0116ea0d0316ed189d7be1e467dc3b79ba1973b017410820e3342f67ffb2fa77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_1a471262da968617d396a43dee4161ed38bb4d1e55bd0d9c517c042e8ef01fee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a471262da968617d396a43dee4161ed38bb4d1e55bd0d9c517c042e8ef01fee->enter($__internal_1a471262da968617d396a43dee4161ed38bb4d1e55bd0d9c517c042e8ef01fee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_0116ea0d0316ed189d7be1e467dc3b79ba1973b017410820e3342f67ffb2fa77->leave($__internal_0116ea0d0316ed189d7be1e467dc3b79ba1973b017410820e3342f67ffb2fa77_prof);

        
        $__internal_1a471262da968617d396a43dee4161ed38bb4d1e55bd0d9c517c042e8ef01fee->leave($__internal_1a471262da968617d396a43dee4161ed38bb4d1e55bd0d9c517c042e8ef01fee_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_72e8765bfa9d5d7796fced6019fc8369997574f8b6a12f7315f1394c4a8fdad3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72e8765bfa9d5d7796fced6019fc8369997574f8b6a12f7315f1394c4a8fdad3->enter($__internal_72e8765bfa9d5d7796fced6019fc8369997574f8b6a12f7315f1394c4a8fdad3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_436bf8ac42add2e83f5bd806cd5b478212185aa454e22f8f48e3d6133c9865d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_436bf8ac42add2e83f5bd806cd5b478212185aa454e22f8f48e3d6133c9865d8->enter($__internal_436bf8ac42add2e83f5bd806cd5b478212185aa454e22f8f48e3d6133c9865d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_436bf8ac42add2e83f5bd806cd5b478212185aa454e22f8f48e3d6133c9865d8->leave($__internal_436bf8ac42add2e83f5bd806cd5b478212185aa454e22f8f48e3d6133c9865d8_prof);

        
        $__internal_72e8765bfa9d5d7796fced6019fc8369997574f8b6a12f7315f1394c4a8fdad3->leave($__internal_72e8765bfa9d5d7796fced6019fc8369997574f8b6a12f7315f1394c4a8fdad3_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_ee6ff07fc5b26bebb146e8b06ec011ad51747037abaf8bcf6bd92a7e73817b5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee6ff07fc5b26bebb146e8b06ec011ad51747037abaf8bcf6bd92a7e73817b5a->enter($__internal_ee6ff07fc5b26bebb146e8b06ec011ad51747037abaf8bcf6bd92a7e73817b5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_55e1e47202f60d34ee388b4cb5da80aee2cbf8ceb1889e9f48480c3770575d1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55e1e47202f60d34ee388b4cb5da80aee2cbf8ceb1889e9f48480c3770575d1b->enter($__internal_55e1e47202f60d34ee388b4cb5da80aee2cbf8ceb1889e9f48480c3770575d1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_55e1e47202f60d34ee388b4cb5da80aee2cbf8ceb1889e9f48480c3770575d1b->leave($__internal_55e1e47202f60d34ee388b4cb5da80aee2cbf8ceb1889e9f48480c3770575d1b_prof);

        
        $__internal_ee6ff07fc5b26bebb146e8b06ec011ad51747037abaf8bcf6bd92a7e73817b5a->leave($__internal_ee6ff07fc5b26bebb146e8b06ec011ad51747037abaf8bcf6bd92a7e73817b5a_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_e97c4689fde718c738790c40d0525b4d780884d0d63e20afc6a5389592cfdca4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e97c4689fde718c738790c40d0525b4d780884d0d63e20afc6a5389592cfdca4->enter($__internal_e97c4689fde718c738790c40d0525b4d780884d0d63e20afc6a5389592cfdca4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_a2e1b91727901f0ad3ef412068b46ac3dbeeb7ba1976a57baade5976b824a7a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2e1b91727901f0ad3ef412068b46ac3dbeeb7ba1976a57baade5976b824a7a9->enter($__internal_a2e1b91727901f0ad3ef412068b46ac3dbeeb7ba1976a57baade5976b824a7a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_a2e1b91727901f0ad3ef412068b46ac3dbeeb7ba1976a57baade5976b824a7a9->leave($__internal_a2e1b91727901f0ad3ef412068b46ac3dbeeb7ba1976a57baade5976b824a7a9_prof);

        
        $__internal_e97c4689fde718c738790c40d0525b4d780884d0d63e20afc6a5389592cfdca4->leave($__internal_e97c4689fde718c738790c40d0525b4d780884d0d63e20afc6a5389592cfdca4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
