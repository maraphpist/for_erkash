<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_62ace0ccecacfdc7c1b686bc690b2519e2504a94b53e1e84759afb9c2465d7d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f452ef2792180f64aba7d2d5cdfc922100138bd05f1e8d32e23bfdff189b50c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f452ef2792180f64aba7d2d5cdfc922100138bd05f1e8d32e23bfdff189b50c->enter($__internal_7f452ef2792180f64aba7d2d5cdfc922100138bd05f1e8d32e23bfdff189b50c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_d1199a0d10ba5499e4c5953e79c90e261123ac351867660c15066e51adfe1d44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1199a0d10ba5499e4c5953e79c90e261123ac351867660c15066e51adfe1d44->enter($__internal_d1199a0d10ba5499e4c5953e79c90e261123ac351867660c15066e51adfe1d44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f452ef2792180f64aba7d2d5cdfc922100138bd05f1e8d32e23bfdff189b50c->leave($__internal_7f452ef2792180f64aba7d2d5cdfc922100138bd05f1e8d32e23bfdff189b50c_prof);

        
        $__internal_d1199a0d10ba5499e4c5953e79c90e261123ac351867660c15066e51adfe1d44->leave($__internal_d1199a0d10ba5499e4c5953e79c90e261123ac351867660c15066e51adfe1d44_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6d7d03460a580b58a76f2f74d77cd5c3740b8c82a4d0eab5185ce41fe266979c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d7d03460a580b58a76f2f74d77cd5c3740b8c82a4d0eab5185ce41fe266979c->enter($__internal_6d7d03460a580b58a76f2f74d77cd5c3740b8c82a4d0eab5185ce41fe266979c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5d1942f27df0ef4b85ba2c589b4f8a9d34f4e6d02d572b3e21fca5293f7c5151 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d1942f27df0ef4b85ba2c589b4f8a9d34f4e6d02d572b3e21fca5293f7c5151->enter($__internal_5d1942f27df0ef4b85ba2c589b4f8a9d34f4e6d02d572b3e21fca5293f7c5151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_5d1942f27df0ef4b85ba2c589b4f8a9d34f4e6d02d572b3e21fca5293f7c5151->leave($__internal_5d1942f27df0ef4b85ba2c589b4f8a9d34f4e6d02d572b3e21fca5293f7c5151_prof);

        
        $__internal_6d7d03460a580b58a76f2f74d77cd5c3740b8c82a4d0eab5185ce41fe266979c->leave($__internal_6d7d03460a580b58a76f2f74d77cd5c3740b8c82a4d0eab5185ce41fe266979c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
