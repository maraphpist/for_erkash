<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_1ffd33fe3ac84b6ab5643eac2d48860af95ab4cdff3de3aee195443cdc8ef51d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0e2d08f1007d5617711e33d16d0489e9e77d0ccfd3309d1679ede7dd49bd9cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0e2d08f1007d5617711e33d16d0489e9e77d0ccfd3309d1679ede7dd49bd9cd->enter($__internal_d0e2d08f1007d5617711e33d16d0489e9e77d0ccfd3309d1679ede7dd49bd9cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_4b6147f875f643b46c8cfa3d0bc184ef14163c95767253f18e47b9e04aec10d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b6147f875f643b46c8cfa3d0bc184ef14163c95767253f18e47b9e04aec10d0->enter($__internal_4b6147f875f643b46c8cfa3d0bc184ef14163c95767253f18e47b9e04aec10d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_d0e2d08f1007d5617711e33d16d0489e9e77d0ccfd3309d1679ede7dd49bd9cd->leave($__internal_d0e2d08f1007d5617711e33d16d0489e9e77d0ccfd3309d1679ede7dd49bd9cd_prof);

        
        $__internal_4b6147f875f643b46c8cfa3d0bc184ef14163c95767253f18e47b9e04aec10d0->leave($__internal_4b6147f875f643b46c8cfa3d0bc184ef14163c95767253f18e47b9e04aec10d0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square.svg");
    }
}
