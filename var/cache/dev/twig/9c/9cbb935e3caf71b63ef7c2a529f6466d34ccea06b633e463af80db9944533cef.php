<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_51037d530ac03d4d38aa2e08187d49dcf965546fc0f13386017a8e92e9ad9e7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0caa9ddac1e373fc95adba85d8caf5b65acd8de847dcd7fe3e1ca84b855ad37e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0caa9ddac1e373fc95adba85d8caf5b65acd8de847dcd7fe3e1ca84b855ad37e->enter($__internal_0caa9ddac1e373fc95adba85d8caf5b65acd8de847dcd7fe3e1ca84b855ad37e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_6f4942d4564ae23f729c82b2a1cafee20b94c69490f5d642a9e113222d713eb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f4942d4564ae23f729c82b2a1cafee20b94c69490f5d642a9e113222d713eb9->enter($__internal_6f4942d4564ae23f729c82b2a1cafee20b94c69490f5d642a9e113222d713eb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_0caa9ddac1e373fc95adba85d8caf5b65acd8de847dcd7fe3e1ca84b855ad37e->leave($__internal_0caa9ddac1e373fc95adba85d8caf5b65acd8de847dcd7fe3e1ca84b855ad37e_prof);

        
        $__internal_6f4942d4564ae23f729c82b2a1cafee20b94c69490f5d642a9e113222d713eb9->leave($__internal_6f4942d4564ae23f729c82b2a1cafee20b94c69490f5d642a9e113222d713eb9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
