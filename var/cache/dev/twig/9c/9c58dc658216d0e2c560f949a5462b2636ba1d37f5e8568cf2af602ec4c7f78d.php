<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_88aa5590e1728b9aa9f696c69b5f470835caa9d563c4be92d673ae82c175976d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53dcc05d5fac57489f17017173b3c7cff9236104edf4a3fa043bb9d99d25e70b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53dcc05d5fac57489f17017173b3c7cff9236104edf4a3fa043bb9d99d25e70b->enter($__internal_53dcc05d5fac57489f17017173b3c7cff9236104edf4a3fa043bb9d99d25e70b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_a0b331d86ba8a28d742d410dca330cca881288fc9600e6e106bc9afa20965a2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0b331d86ba8a28d742d410dca330cca881288fc9600e6e106bc9afa20965a2a->enter($__internal_a0b331d86ba8a28d742d410dca330cca881288fc9600e6e106bc9afa20965a2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_53dcc05d5fac57489f17017173b3c7cff9236104edf4a3fa043bb9d99d25e70b->leave($__internal_53dcc05d5fac57489f17017173b3c7cff9236104edf4a3fa043bb9d99d25e70b_prof);

        
        $__internal_a0b331d86ba8a28d742d410dca330cca881288fc9600e6e106bc9afa20965a2a->leave($__internal_a0b331d86ba8a28d742d410dca330cca881288fc9600e6e106bc9afa20965a2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
