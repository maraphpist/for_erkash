<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_bcee72b5a49fa11ebb864b9c54977b53673b38d5ff421148bb4188ba0eead1f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7267c0f130d85746434971b5873ee9a01c4eb86f29b8bad6e88e273d058c2cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7267c0f130d85746434971b5873ee9a01c4eb86f29b8bad6e88e273d058c2cf->enter($__internal_f7267c0f130d85746434971b5873ee9a01c4eb86f29b8bad6e88e273d058c2cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_99937622ce5331a1285aa24e8cb187ae6b2e1427953528a77577590b1b82a1f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99937622ce5331a1285aa24e8cb187ae6b2e1427953528a77577590b1b82a1f4->enter($__internal_99937622ce5331a1285aa24e8cb187ae6b2e1427953528a77577590b1b82a1f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_f7267c0f130d85746434971b5873ee9a01c4eb86f29b8bad6e88e273d058c2cf->leave($__internal_f7267c0f130d85746434971b5873ee9a01c4eb86f29b8bad6e88e273d058c2cf_prof);

        
        $__internal_99937622ce5331a1285aa24e8cb187ae6b2e1427953528a77577590b1b82a1f4->leave($__internal_99937622ce5331a1285aa24e8cb187ae6b2e1427953528a77577590b1b82a1f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
