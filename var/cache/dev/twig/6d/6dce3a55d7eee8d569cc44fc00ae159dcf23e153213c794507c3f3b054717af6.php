<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_1d248dcab04f8cbde7f9f83d30aa6fe4d619c84bcc9614f7da894e29a15f9d8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_029b49bbe5669f14ffd49e7c90775a3c0eaaac56f5d6ef2074121bc5e0504db5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_029b49bbe5669f14ffd49e7c90775a3c0eaaac56f5d6ef2074121bc5e0504db5->enter($__internal_029b49bbe5669f14ffd49e7c90775a3c0eaaac56f5d6ef2074121bc5e0504db5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_a03ce82775be844b61c0c510fa2a8859588c67997f53419077cd1e3c01e4ddae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a03ce82775be844b61c0c510fa2a8859588c67997f53419077cd1e3c01e4ddae->enter($__internal_a03ce82775be844b61c0c510fa2a8859588c67997f53419077cd1e3c01e4ddae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_029b49bbe5669f14ffd49e7c90775a3c0eaaac56f5d6ef2074121bc5e0504db5->leave($__internal_029b49bbe5669f14ffd49e7c90775a3c0eaaac56f5d6ef2074121bc5e0504db5_prof);

        
        $__internal_a03ce82775be844b61c0c510fa2a8859588c67997f53419077cd1e3c01e4ddae->leave($__internal_a03ce82775be844b61c0c510fa2a8859588c67997f53419077cd1e3c01e4ddae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
