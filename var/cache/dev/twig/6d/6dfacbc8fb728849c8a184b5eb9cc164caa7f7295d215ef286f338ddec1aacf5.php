<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0a150352ead77b8f4825c5cb64eb12a87033c68131590abccc15d5143742aa77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40b8a7566566e13da4250f50210a17d07451efd10cbc22a6269ab79113afb9c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40b8a7566566e13da4250f50210a17d07451efd10cbc22a6269ab79113afb9c3->enter($__internal_40b8a7566566e13da4250f50210a17d07451efd10cbc22a6269ab79113afb9c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_a6e26e8d0ac077c319ab8cb863183d6b0f22355e4407409c746c0f702cb84a9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6e26e8d0ac077c319ab8cb863183d6b0f22355e4407409c746c0f702cb84a9e->enter($__internal_a6e26e8d0ac077c319ab8cb863183d6b0f22355e4407409c746c0f702cb84a9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_40b8a7566566e13da4250f50210a17d07451efd10cbc22a6269ab79113afb9c3->leave($__internal_40b8a7566566e13da4250f50210a17d07451efd10cbc22a6269ab79113afb9c3_prof);

        
        $__internal_a6e26e8d0ac077c319ab8cb863183d6b0f22355e4407409c746c0f702cb84a9e->leave($__internal_a6e26e8d0ac077c319ab8cb863183d6b0f22355e4407409c746c0f702cb84a9e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
