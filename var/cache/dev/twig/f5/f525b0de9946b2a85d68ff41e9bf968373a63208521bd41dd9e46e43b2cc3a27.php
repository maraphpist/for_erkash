<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_3940b12f38f4f22cd7582c0ade22f69829cb0a3d02a402a00fb82a273828a6d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10d6f61aacec05539860d783fa5481a990de1ffd873c493983e499296b526ce3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10d6f61aacec05539860d783fa5481a990de1ffd873c493983e499296b526ce3->enter($__internal_10d6f61aacec05539860d783fa5481a990de1ffd873c493983e499296b526ce3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_dea31f3ee32cf1878225db9aa33f575eb706e74d29adee86bd6ea766faa9c341 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dea31f3ee32cf1878225db9aa33f575eb706e74d29adee86bd6ea766faa9c341->enter($__internal_dea31f3ee32cf1878225db9aa33f575eb706e74d29adee86bd6ea766faa9c341_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_10d6f61aacec05539860d783fa5481a990de1ffd873c493983e499296b526ce3->leave($__internal_10d6f61aacec05539860d783fa5481a990de1ffd873c493983e499296b526ce3_prof);

        
        $__internal_dea31f3ee32cf1878225db9aa33f575eb706e74d29adee86bd6ea766faa9c341->leave($__internal_dea31f3ee32cf1878225db9aa33f575eb706e74d29adee86bd6ea766faa9c341_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5d1fbcec4ad695685b2eb8310bf088797e93ebe567e02f00d3c18eba8405130c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d1fbcec4ad695685b2eb8310bf088797e93ebe567e02f00d3c18eba8405130c->enter($__internal_5d1fbcec4ad695685b2eb8310bf088797e93ebe567e02f00d3c18eba8405130c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ce0d38a7278f178e019f331182704b7620e6e6d99e2c23d95e4ce5a39fe7926b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce0d38a7278f178e019f331182704b7620e6e6d99e2c23d95e4ce5a39fe7926b->enter($__internal_ce0d38a7278f178e019f331182704b7620e6e6d99e2c23d95e4ce5a39fe7926b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_ce0d38a7278f178e019f331182704b7620e6e6d99e2c23d95e4ce5a39fe7926b->leave($__internal_ce0d38a7278f178e019f331182704b7620e6e6d99e2c23d95e4ce5a39fe7926b_prof);

        
        $__internal_5d1fbcec4ad695685b2eb8310bf088797e93ebe567e02f00d3c18eba8405130c->leave($__internal_5d1fbcec4ad695685b2eb8310bf088797e93ebe567e02f00d3c18eba8405130c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
