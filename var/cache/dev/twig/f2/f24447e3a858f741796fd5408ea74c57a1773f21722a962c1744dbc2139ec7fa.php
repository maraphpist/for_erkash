<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_14ee7c3912401387de17c56ebfb16063f7708920acbbc5432689de80cfcc7df9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e02015aeb50ccb5650c08910ac44360003f79b7494ca26e716529beb7cb8b68d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e02015aeb50ccb5650c08910ac44360003f79b7494ca26e716529beb7cb8b68d->enter($__internal_e02015aeb50ccb5650c08910ac44360003f79b7494ca26e716529beb7cb8b68d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_aad85d74857a3e8ea0c51e013d518bde262ef1d6412be5cd4310e7f53eca2762 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aad85d74857a3e8ea0c51e013d518bde262ef1d6412be5cd4310e7f53eca2762->enter($__internal_aad85d74857a3e8ea0c51e013d518bde262ef1d6412be5cd4310e7f53eca2762_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e02015aeb50ccb5650c08910ac44360003f79b7494ca26e716529beb7cb8b68d->leave($__internal_e02015aeb50ccb5650c08910ac44360003f79b7494ca26e716529beb7cb8b68d_prof);

        
        $__internal_aad85d74857a3e8ea0c51e013d518bde262ef1d6412be5cd4310e7f53eca2762->leave($__internal_aad85d74857a3e8ea0c51e013d518bde262ef1d6412be5cd4310e7f53eca2762_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
