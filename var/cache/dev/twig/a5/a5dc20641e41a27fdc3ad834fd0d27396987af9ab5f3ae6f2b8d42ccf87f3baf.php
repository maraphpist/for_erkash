<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_73df4408d4afd78de1a8195505139769de9e3d178bbd706eaf09d5975e7b1cfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b55b4caefea6883a5f1c59a6f37c9471ffad4ee54bdf26123b3bfb761aa8c3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b55b4caefea6883a5f1c59a6f37c9471ffad4ee54bdf26123b3bfb761aa8c3b->enter($__internal_2b55b4caefea6883a5f1c59a6f37c9471ffad4ee54bdf26123b3bfb761aa8c3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_6cfbec96b3f16a7305b1485b6be23681001e96828b379bb12f5d285efac0677a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cfbec96b3f16a7305b1485b6be23681001e96828b379bb12f5d285efac0677a->enter($__internal_6cfbec96b3f16a7305b1485b6be23681001e96828b379bb12f5d285efac0677a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_2b55b4caefea6883a5f1c59a6f37c9471ffad4ee54bdf26123b3bfb761aa8c3b->leave($__internal_2b55b4caefea6883a5f1c59a6f37c9471ffad4ee54bdf26123b3bfb761aa8c3b_prof);

        
        $__internal_6cfbec96b3f16a7305b1485b6be23681001e96828b379bb12f5d285efac0677a->leave($__internal_6cfbec96b3f16a7305b1485b6be23681001e96828b379bb12f5d285efac0677a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
