<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_fb7333f16a6803cc6226c73ee2f5f0e9486aaa095366b8f853b49a2b908671e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_145a2a3b8e273a10254d64ea6d10c4c2f1268fdf5c6e4296b90f2110c4b6e3bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_145a2a3b8e273a10254d64ea6d10c4c2f1268fdf5c6e4296b90f2110c4b6e3bd->enter($__internal_145a2a3b8e273a10254d64ea6d10c4c2f1268fdf5c6e4296b90f2110c4b6e3bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_88c2e1a847046d1efd60f1c420c244b00ac6c0a7ce9aafdd9eea7a31eea82e7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88c2e1a847046d1efd60f1c420c244b00ac6c0a7ce9aafdd9eea7a31eea82e7c->enter($__internal_88c2e1a847046d1efd60f1c420c244b00ac6c0a7ce9aafdd9eea7a31eea82e7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_145a2a3b8e273a10254d64ea6d10c4c2f1268fdf5c6e4296b90f2110c4b6e3bd->leave($__internal_145a2a3b8e273a10254d64ea6d10c4c2f1268fdf5c6e4296b90f2110c4b6e3bd_prof);

        
        $__internal_88c2e1a847046d1efd60f1c420c244b00ac6c0a7ce9aafdd9eea7a31eea82e7c->leave($__internal_88c2e1a847046d1efd60f1c420c244b00ac6c0a7ce9aafdd9eea7a31eea82e7c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_12253d868f3b7fba2aef1c0a4caae59d16ff476ca0ba82ad25693bd363eaa31d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12253d868f3b7fba2aef1c0a4caae59d16ff476ca0ba82ad25693bd363eaa31d->enter($__internal_12253d868f3b7fba2aef1c0a4caae59d16ff476ca0ba82ad25693bd363eaa31d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7d2005462fac103d1e8944cbf4d275bf555cd8ccdeb2c236e61050f18b1c452a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d2005462fac103d1e8944cbf4d275bf555cd8ccdeb2c236e61050f18b1c452a->enter($__internal_7d2005462fac103d1e8944cbf4d275bf555cd8ccdeb2c236e61050f18b1c452a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_7d2005462fac103d1e8944cbf4d275bf555cd8ccdeb2c236e61050f18b1c452a->leave($__internal_7d2005462fac103d1e8944cbf4d275bf555cd8ccdeb2c236e61050f18b1c452a_prof);

        
        $__internal_12253d868f3b7fba2aef1c0a4caae59d16ff476ca0ba82ad25693bd363eaa31d->leave($__internal_12253d868f3b7fba2aef1c0a4caae59d16ff476ca0ba82ad25693bd363eaa31d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
