<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_e12ba50100984e4be16524ff0b359558caf5114763600052ebc819e4740ad228 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b60da7c5403ecf5ee085c96d5a0b0a14114380de10eb20995c25170162fcea0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b60da7c5403ecf5ee085c96d5a0b0a14114380de10eb20995c25170162fcea0e->enter($__internal_b60da7c5403ecf5ee085c96d5a0b0a14114380de10eb20995c25170162fcea0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_c532d79c24ebd766056665a56dc6ae392b047f2b7e14534a32b1ce4607b4fd94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c532d79c24ebd766056665a56dc6ae392b047f2b7e14534a32b1ce4607b4fd94->enter($__internal_c532d79c24ebd766056665a56dc6ae392b047f2b7e14534a32b1ce4607b4fd94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_b60da7c5403ecf5ee085c96d5a0b0a14114380de10eb20995c25170162fcea0e->leave($__internal_b60da7c5403ecf5ee085c96d5a0b0a14114380de10eb20995c25170162fcea0e_prof);

        
        $__internal_c532d79c24ebd766056665a56dc6ae392b047f2b7e14534a32b1ce4607b4fd94->leave($__internal_c532d79c24ebd766056665a56dc6ae392b047f2b7e14534a32b1ce4607b4fd94_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
