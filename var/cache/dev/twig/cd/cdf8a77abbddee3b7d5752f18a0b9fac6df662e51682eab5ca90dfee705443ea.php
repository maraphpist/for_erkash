<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_6e976f8b65a81c21affc1ecaf9a24ba05f917b1e59d98ff62190a6231c84be59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d32de73f9efea12fd31ee05a472a1bf5b4b78e01d43aab383f857b90ef6f0635 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d32de73f9efea12fd31ee05a472a1bf5b4b78e01d43aab383f857b90ef6f0635->enter($__internal_d32de73f9efea12fd31ee05a472a1bf5b4b78e01d43aab383f857b90ef6f0635_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_91c7c97a8fab96db8b65b9333fd173384e6a1e6eba33286820320cb3359e1587 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91c7c97a8fab96db8b65b9333fd173384e6a1e6eba33286820320cb3359e1587->enter($__internal_91c7c97a8fab96db8b65b9333fd173384e6a1e6eba33286820320cb3359e1587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d32de73f9efea12fd31ee05a472a1bf5b4b78e01d43aab383f857b90ef6f0635->leave($__internal_d32de73f9efea12fd31ee05a472a1bf5b4b78e01d43aab383f857b90ef6f0635_prof);

        
        $__internal_91c7c97a8fab96db8b65b9333fd173384e6a1e6eba33286820320cb3359e1587->leave($__internal_91c7c97a8fab96db8b65b9333fd173384e6a1e6eba33286820320cb3359e1587_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_91a17ae96253914430a08f668420c8db6ff4ed4c132df47f2aaca4dadbe4b5d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91a17ae96253914430a08f668420c8db6ff4ed4c132df47f2aaca4dadbe4b5d8->enter($__internal_91a17ae96253914430a08f668420c8db6ff4ed4c132df47f2aaca4dadbe4b5d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8fa7e0b3d1948eee7493132f4d7db9a63c17a6fec35ab195ab8fb197e5d01e24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fa7e0b3d1948eee7493132f4d7db9a63c17a6fec35ab195ab8fb197e5d01e24->enter($__internal_8fa7e0b3d1948eee7493132f4d7db9a63c17a6fec35ab195ab8fb197e5d01e24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_8fa7e0b3d1948eee7493132f4d7db9a63c17a6fec35ab195ab8fb197e5d01e24->leave($__internal_8fa7e0b3d1948eee7493132f4d7db9a63c17a6fec35ab195ab8fb197e5d01e24_prof);

        
        $__internal_91a17ae96253914430a08f668420c8db6ff4ed4c132df47f2aaca4dadbe4b5d8->leave($__internal_91a17ae96253914430a08f668420c8db6ff4ed4c132df47f2aaca4dadbe4b5d8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
