<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_9c4f657469d396758145b0b399b9355e90943c3e96346630fc1eab2a16180b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01e92eb5a7d0dbc1fc0cd7459118ebbcd8463c4eeed91fb98ba03b7914b03b98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01e92eb5a7d0dbc1fc0cd7459118ebbcd8463c4eeed91fb98ba03b7914b03b98->enter($__internal_01e92eb5a7d0dbc1fc0cd7459118ebbcd8463c4eeed91fb98ba03b7914b03b98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_2e5e589df2ae868a5706af6c2c1c0d12eafda572ac05c8575802c2eca3c0e0d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e5e589df2ae868a5706af6c2c1c0d12eafda572ac05c8575802c2eca3c0e0d3->enter($__internal_2e5e589df2ae868a5706af6c2c1c0d12eafda572ac05c8575802c2eca3c0e0d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_01e92eb5a7d0dbc1fc0cd7459118ebbcd8463c4eeed91fb98ba03b7914b03b98->leave($__internal_01e92eb5a7d0dbc1fc0cd7459118ebbcd8463c4eeed91fb98ba03b7914b03b98_prof);

        
        $__internal_2e5e589df2ae868a5706af6c2c1c0d12eafda572ac05c8575802c2eca3c0e0d3->leave($__internal_2e5e589df2ae868a5706af6c2c1c0d12eafda572ac05c8575802c2eca3c0e0d3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
