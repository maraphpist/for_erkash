<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_119c72e7dff84766de0fd2a32db5fd6c286207212ae9347f60b3d1e16520fd97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9581d7e36347242094c0454e59ffece6d78d751ab8a480c764f1668d342e0b7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9581d7e36347242094c0454e59ffece6d78d751ab8a480c764f1668d342e0b7a->enter($__internal_9581d7e36347242094c0454e59ffece6d78d751ab8a480c764f1668d342e0b7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_daba4655d151fb40a5d59878c74cdd1c638f9393f3a408cc6f969720e3cc301a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_daba4655d151fb40a5d59878c74cdd1c638f9393f3a408cc6f969720e3cc301a->enter($__internal_daba4655d151fb40a5d59878c74cdd1c638f9393f3a408cc6f969720e3cc301a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_9581d7e36347242094c0454e59ffece6d78d751ab8a480c764f1668d342e0b7a->leave($__internal_9581d7e36347242094c0454e59ffece6d78d751ab8a480c764f1668d342e0b7a_prof);

        
        $__internal_daba4655d151fb40a5d59878c74cdd1c638f9393f3a408cc6f969720e3cc301a->leave($__internal_daba4655d151fb40a5d59878c74cdd1c638f9393f3a408cc6f969720e3cc301a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
