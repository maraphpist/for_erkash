<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_9f4f286450227213bf58a0359df4d302b4fd04e289a400a5467910f0c3276bf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cf01db7fe792ad3721b208029e39ce4f5074993781ecbec7fd1c52908d0c2d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cf01db7fe792ad3721b208029e39ce4f5074993781ecbec7fd1c52908d0c2d0->enter($__internal_3cf01db7fe792ad3721b208029e39ce4f5074993781ecbec7fd1c52908d0c2d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_5aeda1eb777c6963dc44071977f0f4a98b2549e89a1c522a7634843aaa573d7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aeda1eb777c6963dc44071977f0f4a98b2549e89a1c522a7634843aaa573d7a->enter($__internal_5aeda1eb777c6963dc44071977f0f4a98b2549e89a1c522a7634843aaa573d7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3cf01db7fe792ad3721b208029e39ce4f5074993781ecbec7fd1c52908d0c2d0->leave($__internal_3cf01db7fe792ad3721b208029e39ce4f5074993781ecbec7fd1c52908d0c2d0_prof);

        
        $__internal_5aeda1eb777c6963dc44071977f0f4a98b2549e89a1c522a7634843aaa573d7a->leave($__internal_5aeda1eb777c6963dc44071977f0f4a98b2549e89a1c522a7634843aaa573d7a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b1082e45b770420ec4c3e47d878826e7f36ab003900c3cf50c1f16fa6c59b140 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1082e45b770420ec4c3e47d878826e7f36ab003900c3cf50c1f16fa6c59b140->enter($__internal_b1082e45b770420ec4c3e47d878826e7f36ab003900c3cf50c1f16fa6c59b140_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5500cb0a5e379dfc0d9dcf5f53065376d14efbdb8f148d4ebfab5155a68cf095 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5500cb0a5e379dfc0d9dcf5f53065376d14efbdb8f148d4ebfab5155a68cf095->enter($__internal_5500cb0a5e379dfc0d9dcf5f53065376d14efbdb8f148d4ebfab5155a68cf095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_5500cb0a5e379dfc0d9dcf5f53065376d14efbdb8f148d4ebfab5155a68cf095->leave($__internal_5500cb0a5e379dfc0d9dcf5f53065376d14efbdb8f148d4ebfab5155a68cf095_prof);

        
        $__internal_b1082e45b770420ec4c3e47d878826e7f36ab003900c3cf50c1f16fa6c59b140->leave($__internal_b1082e45b770420ec4c3e47d878826e7f36ab003900c3cf50c1f16fa6c59b140_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
