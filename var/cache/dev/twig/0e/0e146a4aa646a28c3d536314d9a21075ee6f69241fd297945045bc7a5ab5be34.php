<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_6be2fa8c026f231cc0acc0b18d7daeaf8f7b74e8b59c58c4285bb6535f71616a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2da01bee7dde7007418b405295d812816a2419ec598b13ec889e6be12b347458 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2da01bee7dde7007418b405295d812816a2419ec598b13ec889e6be12b347458->enter($__internal_2da01bee7dde7007418b405295d812816a2419ec598b13ec889e6be12b347458_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_60e987d38a35b6ba0a695637ab80165bc8e64be51cb13be2b43831076ee556be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60e987d38a35b6ba0a695637ab80165bc8e64be51cb13be2b43831076ee556be->enter($__internal_60e987d38a35b6ba0a695637ab80165bc8e64be51cb13be2b43831076ee556be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_2da01bee7dde7007418b405295d812816a2419ec598b13ec889e6be12b347458->leave($__internal_2da01bee7dde7007418b405295d812816a2419ec598b13ec889e6be12b347458_prof);

        
        $__internal_60e987d38a35b6ba0a695637ab80165bc8e64be51cb13be2b43831076ee556be->leave($__internal_60e987d38a35b6ba0a695637ab80165bc8e64be51cb13be2b43831076ee556be_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_39a248f67323eddcc9a1807f42e5c85079a3afaa93cf4c38fb40fe71ef11418a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39a248f67323eddcc9a1807f42e5c85079a3afaa93cf4c38fb40fe71ef11418a->enter($__internal_39a248f67323eddcc9a1807f42e5c85079a3afaa93cf4c38fb40fe71ef11418a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_29587303fa10b7dbc7ba2e6da29f831b6a3bdc3442c78f4682c6a12edc1964a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29587303fa10b7dbc7ba2e6da29f831b6a3bdc3442c78f4682c6a12edc1964a7->enter($__internal_29587303fa10b7dbc7ba2e6da29f831b6a3bdc3442c78f4682c6a12edc1964a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_29587303fa10b7dbc7ba2e6da29f831b6a3bdc3442c78f4682c6a12edc1964a7->leave($__internal_29587303fa10b7dbc7ba2e6da29f831b6a3bdc3442c78f4682c6a12edc1964a7_prof);

        
        $__internal_39a248f67323eddcc9a1807f42e5c85079a3afaa93cf4c38fb40fe71ef11418a->leave($__internal_39a248f67323eddcc9a1807f42e5c85079a3afaa93cf4c38fb40fe71ef11418a_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_87ed1ed5030a0d1105eac7164bbd53a2c4f3ef2fd2c0cf8a305fbffbd06e48cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87ed1ed5030a0d1105eac7164bbd53a2c4f3ef2fd2c0cf8a305fbffbd06e48cf->enter($__internal_87ed1ed5030a0d1105eac7164bbd53a2c4f3ef2fd2c0cf8a305fbffbd06e48cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_e7b2b8c49ebf19450b7a0541c7f64d055b691606018469c3ed972129f8d82ad6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7b2b8c49ebf19450b7a0541c7f64d055b691606018469c3ed972129f8d82ad6->enter($__internal_e7b2b8c49ebf19450b7a0541c7f64d055b691606018469c3ed972129f8d82ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_e7b2b8c49ebf19450b7a0541c7f64d055b691606018469c3ed972129f8d82ad6->leave($__internal_e7b2b8c49ebf19450b7a0541c7f64d055b691606018469c3ed972129f8d82ad6_prof);

        
        $__internal_87ed1ed5030a0d1105eac7164bbd53a2c4f3ef2fd2c0cf8a305fbffbd06e48cf->leave($__internal_87ed1ed5030a0d1105eac7164bbd53a2c4f3ef2fd2c0cf8a305fbffbd06e48cf_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_5e54f22e82e7a851161d11b5325c2c588d952ada2b605bf16e454e17b38a886a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e54f22e82e7a851161d11b5325c2c588d952ada2b605bf16e454e17b38a886a->enter($__internal_5e54f22e82e7a851161d11b5325c2c588d952ada2b605bf16e454e17b38a886a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_245174e011cf715dee156ec0e57d4574231c0d31fea2bb38bea729ea9a7e595f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_245174e011cf715dee156ec0e57d4574231c0d31fea2bb38bea729ea9a7e595f->enter($__internal_245174e011cf715dee156ec0e57d4574231c0d31fea2bb38bea729ea9a7e595f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_245174e011cf715dee156ec0e57d4574231c0d31fea2bb38bea729ea9a7e595f->leave($__internal_245174e011cf715dee156ec0e57d4574231c0d31fea2bb38bea729ea9a7e595f_prof);

        
        $__internal_5e54f22e82e7a851161d11b5325c2c588d952ada2b605bf16e454e17b38a886a->leave($__internal_5e54f22e82e7a851161d11b5325c2c588d952ada2b605bf16e454e17b38a886a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
