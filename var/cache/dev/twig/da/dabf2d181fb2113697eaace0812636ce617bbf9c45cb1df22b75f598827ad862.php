<?php

/* :default:index.html.twig */
class __TwigTemplate_e7dd53a1d59c18ca9afe194e1bc787facc3052bd1f139ee8df3eac6ed5269f37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6d88ea7a50bc7f547f3c96a74106b64d9928baea8193816dc93bcbf2077d9a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6d88ea7a50bc7f547f3c96a74106b64d9928baea8193816dc93bcbf2077d9a0->enter($__internal_c6d88ea7a50bc7f547f3c96a74106b64d9928baea8193816dc93bcbf2077d9a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $__internal_200ddf567a779869183a73db5cc8f51279fb521d61051a9e73a0d79301412f56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_200ddf567a779869183a73db5cc8f51279fb521d61051a9e73a0d79301412f56->enter($__internal_200ddf567a779869183a73db5cc8f51279fb521d61051a9e73a0d79301412f56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6d88ea7a50bc7f547f3c96a74106b64d9928baea8193816dc93bcbf2077d9a0->leave($__internal_c6d88ea7a50bc7f547f3c96a74106b64d9928baea8193816dc93bcbf2077d9a0_prof);

        
        $__internal_200ddf567a779869183a73db5cc8f51279fb521d61051a9e73a0d79301412f56->leave($__internal_200ddf567a779869183a73db5cc8f51279fb521d61051a9e73a0d79301412f56_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5cab52c34a178c836923e2c546ab80f898ee9ad09ff2afe037d8aff07668cefe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5cab52c34a178c836923e2c546ab80f898ee9ad09ff2afe037d8aff07668cefe->enter($__internal_5cab52c34a178c836923e2c546ab80f898ee9ad09ff2afe037d8aff07668cefe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f980941986f76ff2662e26ab6cb7f9104441c74d6da94ff06ab56de841820b9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f980941986f76ff2662e26ab6cb7f9104441c74d6da94ff06ab56de841820b9f->enter($__internal_f980941986f76ff2662e26ab6cb7f9104441c74d6da94ff06ab56de841820b9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Привет, ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "name", array()), "html", null, true);
        echo "</h1>
";
        
        $__internal_f980941986f76ff2662e26ab6cb7f9104441c74d6da94ff06ab56de841820b9f->leave($__internal_f980941986f76ff2662e26ab6cb7f9104441c74d6da94ff06ab56de841820b9f_prof);

        
        $__internal_5cab52c34a178c836923e2c546ab80f898ee9ad09ff2afe037d8aff07668cefe->leave($__internal_5cab52c34a178c836923e2c546ab80f898ee9ad09ff2afe037d8aff07668cefe_prof);

    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block body %}
    <h1>Привет, {{ user.name }}</h1>
{% endblock %}
", ":default:index.html.twig", "/var/www/html/self_dev/symfony_lessons/for_erkash/app/Resources/views/default/index.html.twig");
    }
}
